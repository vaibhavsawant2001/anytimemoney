<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Site extends CI_Controller
{

	var $data = array();
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Site_model');
		// Load session library
		$this->load->library('session');
		$this->CI = &get_instance();
	}

	public function index()
	{
		$data['categories'] = $this->Site_model->select_category();
		$data['seoData'] = $this->Site_model->select_seo('Home');
		$data['seo'] = $data['seoData'][0] ?? '';
		$this->load->view('header', $data);
		$this->load->view('home', $data);
		$this->load->view('footer', $data);
	}

	// -------------------------Products---------------------------------------------

	public function product_detail($id)
	{
		$data['product_single'] = $this->db->get_where('products', ['id' => $id, 'status' => '1'])->result();
		$data['product'] = $data['product_single'][0] ?? '';
		$category_id = $data['product_single'][0]->category_id ?? '';
		$remove_id = $data['product_single'][0]->id ?? '';
		$data['similar_products'] = $this->db->order_by('id', 'desc')->where_not_in('id', $remove_id)->get_where('products', ['category_id' => $category_id, 'status' => '1'])->result();
		$data['seoData'] = $this->Site_model->select_seo('Products_details');
		$data['seo'] = $data['seoData'][0] ?? '';
		$this->load->view('header', $data);
		$this->load->view('product_details', $data);
		$this->load->view('footer', $data);
	}

	public function products()
	{
		if (!empty($this->input->get('filter'))) {
			$category = $this->input->get('category_id');

			if (!empty($category)) {
				$data['products_category'] = $this->db->order_by('id', 'desc')->where_in('category_id', $category)->get_where('products', ['status' => '1'])->result();
			} else {
				$data['products_category'] = $this->db->order_by('id', 'desc')->get_where('products', ['status' => '1'])->result();
			}
		} else if (!empty($this->input->get('category_id'))) {
			$data['products_category'] = $this->db->order_by('id', 'desc')->where_in('category_id', $this->input->get('category_id'))->get_where('products', ['status' => '1'])->result();
		} else {
			$data['products_category'] = $this->db->order_by('id', 'desc')->get_where('products', ['status' => '1'])->result();
		}

		$data['categories'] = $this->Site_model->select_category();
		$data['seoData'] = $this->Site_model->select_seo('Product');
		$data['seo'] = $data['seoData'][0] ?? '';
		$this->load->view('header', $data);
		$this->load->view('products', $data);
		$this->load->view('footer', $data);
	}

	//-----------------------favourite-------------------------------------------------
	public function favorite()
	{
		$data['user_id'] = $this->check_user_validation();
		$user_id = $data['user_id'];
		$products = $this->db->order_by('id', 'desc')->get_where('fav_list', ['login_id' => $user_id, 'status' => '1'])->result();

		$new_id = [];
		if (count($products) > 0) {
			foreach ($products as $value) {
				$new_id[] = $value->car_id;
			}
		}

		if (!empty($new_id)) {
			$data['products_category'] = $this->db->order_by('id', 'desc')->where_in('id', $new_id)->get_where('products', ['status' => '1'])->result();
		} else {
			$data['products_category'] = [];
		}

		$data['categories'] = $this->Site_model->select_category();
		$data['seoData'] = $this->Site_model->select_seo('fav');
		$data['seo'] = $data['seoData'][0] ?? '';
		$this->load->view('header', $data);
		$this->load->view('favourite', $data);
		$this->load->view('footer', $data);
	}

	public function fav_add()
	{
		$user_id = $this->check_user_validation();
		$db = get_instance()->db->conn_id;
		// grab user input
		$product_id = mysqli_real_escape_string($db, $this->security->xss_clean($this->input->post('product_id')));
		$data = array(
			'login_id' => $user_id,
			'car_id' => $product_id,
		);

		$run = $this->db->insert('fav_list', $data);
		if ($run) {
			$this->session->set_flashdata('success', 'Successfully Added To Your Favorite List');

			redirect(base_url('shop-detail/' . $product_id));
		} else {
			$this->session->set_flashdata('error', 'Something went wrong, please try again');

			redirect(base_url('shop-detail/' . $product_id));
		}
	}

	public function fav_delete()
	{
		$user_id = $this->check_user_validation();
		$db = get_instance()->db->conn_id;
		// grab user input
		$product_id = mysqli_real_escape_string($db, $this->security->xss_clean($this->input->post('product_id')));

		$this->db->where('car_id', $product_id);
		$this->db->where('login_id', $user_id);
		$run = $this->db->delete('fav_list');
		if ($run) {
			$this->session->set_flashdata('success', 'Successfully removed from Your Favorite List');

			redirect(base_url('shop-detail/' . $product_id));
		} else {
			$this->session->set_flashdata('error', 'Something went wrong, please try again');

			redirect(base_url('shop-detail/' . $product_id));
		}
	}

	//-----------------------My Shopping-------------------------------------------------
	public function my_shopping()
	{
		$data['user_id'] = $this->check_user_validation();
		$user_id = $data['user_id'];

		$this->db->select('*');
		$this->db->from('my_product');
		$this->db->join('products', 'products.id = my_product.product_id');
		$this->db->where('my_product.user_id', $user_id);
		$this->db->where('my_product.status', 1);
		$query = $this->db->get();

		$data['products_category'] = $query->result();
		$data['categories'] = $this->Site_model->select_category();
		$data['seoData'] = $this->Site_model->select_seo('my_shopping');
		$data['seo'] = $data['seoData'][0] ?? '';
		$this->load->view('header', $data);
		$this->load->view('my_shopping', $data);
		$this->load->view('footer', $data);
	}

	//----------------------------Checkout------------------------------------ 

	public function checkout($product_id)
	{
		$data['product_id'] = $product_id ?? '';
		$data['user_id'] = $this->check_user_validation();
		$user_id = $data['user_id'];

		$data['seoData'] = $this->Site_model->select_seo('Checkout');
		$data['seo'] = $data['seoData'][0] ?? '';
		$this->load->view('header', $data);
		$this->load->view('checkout', $data);
		$this->load->view('footer', $data);
	}

	public function checkout_upload()
	{
		$data['user_id'] = $this->check_user_validation();
		$user_id = $data['user_id'];
		$db = get_instance()->db->conn_id;
		$product_id = mysqli_real_escape_string($db, $this->security->xss_clean($this->input->post('product_id')));
		$products = $this->db->get_where('products', ['id' => $product_id, 'status' => '1'])->result();
		$jems = $products[0]->jems ?? 0;
		$amounts = $products[0]->amount ?? 0;

		if (!empty($_FILES['image']['name'])) {
			$type = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION) ?? '';

			if ($type != 'pdf' && $type != 'jpeg' && $type != 'png' && $type != 'jpg') {
				$this->session->set_flashdata('error', 'Only jpeg,pdf,png and jpg files are allowed');
				redirect(base_url('checkout/' . $product_id));
				exit;
			}

			$_FILES['file']['name'] = $_FILES['image']['name'];
			$_FILES['file']['type'] = $_FILES['image']['type'];
			$_FILES['file']['tmp_name'] = $_FILES['image']['tmp_name'];
			$_FILES['file']['error'] = $_FILES['image']['error'];
			$_FILES['file']['size'] = $_FILES['image']['size'];

			// File upload configuration
			$new_name = time() . '_' . $_FILES["image"]['name'];
			$config['file_name'] = $new_name;
			$uploadPath = $_SERVER['DOCUMENT_ROOT'] . str_replace(basename($_SERVER['SCRIPT_NAME']), '', $_SERVER['SCRIPT_NAME']) . 'uploads/file/';
			// $uploadPath = '../uploads/file/';
			$config['upload_path'] = $uploadPath;
			$config['allowed_types'] = 'pdf|jpeg|png|jpg';
			chmod($uploadPath, 0777);

			// Load and initialize upload library
			$this->load->library('upload', $config);
			$this->upload->initialize($config);

			// Upload file to server
			if ($this->upload->do_upload('file')) {
				$fileData = $this->upload->data();
				$imgData = $fileData['file_name'];
				$data = array(
					'file' => $imgData,
					'user_id' => $user_id,
					'product_id' => $product_id,
					'jems' => $jems
				);

				$run = $this->db->insert('my_product', $data);
				if ($run) {
					$data = ['user_id' => $user_id, 'price_id' => '', 'product_id' => $product_id, 'points' => '', 'game_id' => '', 'jems' => $jems, 'amount' => $amounts, 'new_points' => '', 'status' => 'Approval Request Sent'];

					$this->logs($data);

					$this->session->set_flashdata('success', 'Successfully upload the recieve, we will reach out you soon.');

					redirect(base_url('checkout/' . $product_id));
				} else {
					$this->session->set_flashdata('error', 'Something went wrong, please try again');

					redirect(base_url('checkout/' . $product_id));
				}

			} else {
				$this->session->set_flashdata('error', $data['imageError'] = $this->upload->display_errors());

				redirect(base_url('checkout/' . $product_id));
			}

		}
	}

	//-------------------------Prize------------------------------------------------
	public function game_detail($id)
	{
		$data['product_single'] = $this->db->get_where('games', ['id' => $id, 'status' => '1'])->result();
		$data['product'] = $data['product_single'][0] ?? '';
		$remove_id = $data['product_single'][0]->id ?? '';
		$data['similar_products'] = $this->db->order_by('id', 'desc')->where_not_in('id', $remove_id)->get_where('games', ['status' => '1'])->result();
		$data['seoData'] = $this->Site_model->select_seo('Games_details');
		$data['seo'] = $data['seoData'][0] ?? '';
		$this->load->view('header', $data);
		$this->load->view('games_details', $data);
		$this->load->view('footer', $data);
	}

	public function game()
	{
		if (!empty($this->input->get('filter'))) {
			$games = $this->input->get('game_id');

			if (!empty($games)) {
				if ($games <= 20000) {
					$data['products_category'] = $this->db->order_by('id', 'desc')->where('amount >=', 1)->where('amount <=', 20000)->get_where('games', ['status' => '1'])->result();
				} else if ($games >= 20001 && $games <= 40000) {
					$data['products_category'] = $this->db->order_by('id', 'desc')->where('amount >=', 20001)->where('amount <=', 40000)->get_where('games', ['status' => '1'])->result();
				} else {
					$data['products_category'] = $this->db->order_by('id', 'desc')->where('amount >=', 40001)->get_where('games', ['status' => '1'], 3)->result();
				}
			} else {
				$data['products_category'] = $this->db->order_by('id', 'desc')->get_where('games', ['status' => '1'])->result();
			}
		} else if (!empty($this->input->get('game_id'))) {
			$data['products_category'] = $this->db->order_by('id', 'desc')->where('amount<', $this->input->get('game_id'))->get_where('games', ['status' => '1'])->result();
		} else {
			$data['products_category'] = $this->db->order_by('id', 'desc')->get_where('games', ['status' => '1'])->result();
		}

		$data['seoData'] = $this->Site_model->select_seo('Game');
		$data['seo'] = $data['seoData'][0] ?? '';
		$this->load->view('header', $data);
		$this->load->view('games', $data);
		$this->load->view('footer', $data);
	}

	public function price_add()
	{
		$data['user_id'] = $this->check_user_validation();
		$user_id = $data['user_id'];
		$db = get_instance()->db->conn_id;
		$game_id = mysqli_real_escape_string($db, $this->security->xss_clean($this->input->post('game_id')));
		$products = $this->db->get_where('games', ['id' => $game_id, 'status' => '1'])->result();
		$points = $products[0]->points ?? 0;

		$point_register = $this->db->select('total_points')->get_where('register', ['status' => 1, 'register_id' => $user_id])->result();
		$user_point = $point_register[0]->total_points;

		if ($user_point < $points) {
			$this->session->set_flashdata('error', "You don't have enough points to claim this price.");
			redirect(base_url('game-detail/' . $game_id));
		}

		$data = array(
			'user_id' => $user_id,
			'price_id' => $game_id,
			'points' => $points,
		);

		$run = $this->db->insert('my_price', $data);

		if ($run) {
			$new_points = intval($user_point) - intval($points);

			$this->db->where(['status' => '1', 'register_id' => $user_id]);
			$this->db->update('register', ['total_points' => $new_points]);

			$data = ['user_id' => $user_id, 'price_id' => $game_id, 'product_id' => '', 'points' => $points, 'game_id' => '', 'jems' => '', 'amount' => '', 'new_points' => $new_points, 'status' => 'Claimed the price'];

			$this->logs($data);

			$this->session->set_flashdata('success', 'Successfully claimed the price');

			redirect(base_url('game-detail/' . $game_id));
		} else {
			$this->session->set_flashdata('error', 'Something went wrong, please try again');
			redirect(base_url('game-detail/' . $game_id));
		}
	}

	//------------------ for contact us form controller-------------------------------
	public function contactUs()
	{
		$data['seoData'] = $this->Site_model->select_seo('Contact Us');
		$data['seo'] = $data['seoData'][0] ?? '';
		$this->load->view('header', $data);
		$this->load->view('contactUs', $data);
		$this->load->view('footer', $data);
	}

	public function contactdb()
	{
		$url_page = base_url() . "contact-us";
		$ip = $_SERVER['REMOTE_ADDR'];
		$this->form_validation->set_rules('name', 'Title Required', 'required');
		$this->form_validation->set_rules('email', 'Title Required', 'required');
		$this->form_validation->set_rules('message', 'Title Required', 'required');

		if ($this->form_validation->run()) {
			$name = $this->security->xss_clean($this->input->post('name'));
			$email = $this->security->xss_clean($this->input->post('email'));
			$message = $this->security->xss_clean($this->input->post('message'));

			$data = array(
				'name' => $name,
				'email' => $email,
				'message' => $message,
				'content' => '',
				'type' => 'contact',
				'date' => date('Y-m-d'),
				'status' => '1',
				'Page_url' => $url_page,
				'ip_address' => $ip,
			);
			$run = $this->Site_model->contactSubmit($data);
			if ($run) {
				if (isset($_SESSION['message_fail'])) {
					unset($_SESSION['message_fail']);
				}
				/* ------------------------- Mail --------------------------- */
				// for admin
				$to = 'anytimemoney@gmail.com';
				$body = "Hello Sir, <br/>  Have a new Enquiry from Contact Page <br/> ";
				$body .= "Student Name : " . $name . "\r\n<br/>";
				$body .= "Email : " . $email . "\r\n <br/>";
				$body .= "Message : " . $message . "\r\n <br/>	";
				$body .= "Ip Address: " . $ip . "\r\n <br/>";
				$body .= "Url: " . $url_page . "\r\n <br/>";
				$msubject = "New Enquiry from Any Time Money Contact Page";
				$headers = "From: noreply@anytimemoney.com";
				$headers .= "MIME-Version: 1.0\r\n";
				$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

				// for student
				$to2 = $email;
				$body2 = "Hello $name, <br/>  Thank You For Getting in Touch! <br/>  From: Any Time Money <br/> ";
				$msubject2 = "Thank You From Any Time Money";
				$headers2 = "From: noreply@anytimemoney.com";
				$headers2 .= "MIME-Version: 1.0\r\n";
				$headers2 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

				// echo $body.'<br>'.$body2;
				// exit();
				if (@mail($to, $msubject, $body, $headers)) {
					@mail($to2, $msubject2, $body2, $headers2);
					//echo "send";
					// exit();
				} else {
					echo "fail";
					// exit();
				}
				/* ------------------------Mail ----------------------------- */
				$this->session->set_flashdata('message_succ', 'Thank you for Contacting Us , We will get back to you. soon.');
				redirect(base_url('contact-us'));
			} else {

				$this->session->set_flashdata('message_fail', 'Something went wrong.');
				redirect(base_url('contact-us'));
			}

		} else {
			$this->session->set_flashdata('message_fail', 'All field  is required');
			redirect(base_url('contact-us'));
		}
	}

	//===========================================Transaction===================================

	public function transaction()
	{
		$data['user_id'] = $this->check_user_validation();
		$user_id = $data['user_id'];

		$data['seoData'] = $this->Site_model->select_seo('transaction');
		$data['seo'] = $data['seoData'][0] ?? '';
		$this->load->view('header', $data);
		$this->load->view('transaction', $data);
		$this->load->view('footer', $data);
	}

	public function logs($data)
	{
		$data = array(
			'user_id' => $data['user_id'],
			'price_id' => $data['price_id'] ?? '',
			'product_id' => $data['product_id'] ?? '',
			'points' => $data['points'] ?? '',
			'new_points' => $data['new_points'] ?? '',
			'game_id' => $data['game_id'] ?? '',
			'jems' => $data['jems'] ?? '',
			'amount' => $data['amount'] ?? '',
			'status' => $data['status'] ?? '',
		);

		$this->db->insert('logs', $data);
	}

	// Check whether user id there or not
	public function check_user_validation()
	{
		$user_id = $this->session->userdata('register_id');

		if (empty($user_id) && $user_id == null) {
			redirect(base_url());
		}

		return $user_id;
	}
}