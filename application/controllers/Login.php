<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{


	public function __construct()
	{
		parent::__construct();
		$data = array();
		$this->load->model('Login_model');
		$this->load->model('Site_model');
		$this->load->library('session');
		$this->CI = &get_instance();
	}

	// Show login page
	public function index()
	{
		$data['seoData'] = $this->Site_model->select_seo('login');
		$data['seo'] = $data['seoData'][0];
		$this->load->view('header', $data);
		$this->load->view('login');
		$this->load->view('footer');
	}

	public function register()
	{
		$data['seoData'] = $this->Site_model->select_seo('register');
		$data['seo'] = $data['seoData'][0];
		$this->load->view('header', $data);
		$this->load->view('register');
		$this->load->view('footer');
	}
	public function user_login()
	{
		$this->form_validation->set_rules('email', 'Email', 'required');
		$this->form_validation->set_rules('password', 'password', 'required');
		if ($this->form_validation->run()) {
			$db = get_instance()->db->conn_id;
			// grab user input
			$username = mysqli_real_escape_string($db, $this->security->xss_clean($this->input->post('email')));
			$password = mysqli_real_escape_string($db, $this->security->xss_clean($this->input->post('password')));

			$data = array(
				'email' => $username,
				'password' =>$password
			);

			$run = $this->Login_model->login_entry($data);
			if ($run) {
				$user_id = $this->db->insert_id();

				$data = ['user_id' => $user_id, 'price_id' => '', 'product_id' => '', 'points' => '', 'game_id' => '', 'jems' => '', 'amount' => '', 'new_points' => '', 'status' => 'login'];

				$this->logs($data);

				redirect(base_url());
			} else {
				$this->session->set_flashdata('incorrect', 'Email or password is invalid');
				redirect(base_url('Login'));
			}

		} else {
			$this->session->set_flashdata('incorrect', 'Email and password required');
			redirect(base_url('Login'));
		}

	}


	public function logout()
	{
		$user_id = $this->session->userdata('register_id') ?? '';

		$data = ['user_id' => $user_id, 'price_id' => '', 'product_id' => '', 'points' => '', 'game_id' => '', 'jems' => '', 'amount' => '', 'new_points' => '', 'status' => 'logout'];

		$this->logs($data);

		$this->session->sess_destroy();
		redirect(base_url());
	}

	public function forgot()
	{
		$data['seoData'] = $this->Site_model->select_seo('forgot');
		$data['seo'] = $data['seoData'][0];
		$this->load->view('header', $data);
		$this->load->view('forget', $data);
		$this->load->view('footer', $data);
	}

	public function user_forget_password()
	{
		$this->form_validation->set_rules('email', 'Email', 'required');
		if ($this->form_validation->run()) {
			$db = get_instance()->db->conn_id;
			// grab user input
			$username = mysqli_real_escape_string($db, $this->security->xss_clean($this->input->post('email')));

			$email = $this->db->get_where('register', array('email' => $username, 'status' => '1'))->result();
			if (count($email) > 0) {
				$this->db->where('status', '1');
				$this->db->where('email', $username);
				$runs = $this->db->get('register')->result();
				if ($runs) {

					$to2 = $username;
					$body2 = "Hello , <br/>  Thank You For Getting in Touch! <br/> Your  Password is <b>" . $runs[0]->passwords . "</b> <br>  From: Any Time Money <br/> ";
					$msubject2 = "Forget Password From Any Time Money";
					$headers2 = "From: noreply@anytimemoney.com";
					$headers2 .= "MIME-Version: 1.0\r\n";
					$headers2 .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

					@mail($to2, $msubject2, $body2, $headers2);

					$this->session->set_flashdata('correct', 'Your password has been successfully sent to your registered email, please check your scam if you don\'t get email in inbox.');

					$user_id = $runs[0]->register_id ??'';
					$data = ['user_id' => $user_id, 'price_id' => '', 'product_id' => '', 'points' => '', 'game_id' => '', 'jems' => '', 'amount' => '', 'new_points' => '', 'status' => 'forgot password'];

					$this->logs($data);
					
					redirect(base_url('forget'));
				} else {
					$this->session->set_flashdata('incorrect', 'Something Went Wrong');
					redirect(base_url('forget'));
				}
			} else {
				$this->session->set_flashdata('incorrect', 'Email not Exist');
				redirect(base_url('forget'));
			}
		} else {
			$this->session->set_flashdata('incorrect', 'Email and password required');
			redirect(base_url('forget'));
		}

	}

	public function registrations()
	{
		$db = get_instance()->db->conn_id;
		$name = mysqli_real_escape_string($db, $this->security->xss_clean($this->input->post('name')));
		$email = mysqli_real_escape_string($db, $this->security->xss_clean($this->input->post('email')));
		$phone = mysqli_real_escape_string($db, $this->security->xss_clean($this->input->post('phone')));
		$city = mysqli_real_escape_string($db, $this->security->xss_clean($this->input->post('city')));
		$password = mysqli_real_escape_string($db, $this->security->xss_clean($this->input->post('password')));

		$emails = $this->db->get_where('register', array('email' => $email, 'passwords' => $password, 'status' => '1'))->result();

		if (count($emails) > 0) {
			$this->session->set_flashdata('danger', 'Email Already Exists.');
			$data['seoData'] = $this->Site_model->select_seo('login');
			$data['seo'] = $data['seoData'][0];
			$data['control'] = $this->input->post();
			$this->load->view('header', $data);
			$this->load->view('register', $data);
			$this->load->view('footer');
		} else {
			$data = array(
				'name' => $name,
				'email' => $email,
				'mobile' => $phone,
				'city' => $city,
				'password' => $password,
				'passwords' => $password,
				'status' => 1,
				'user_generated_id' => time(),
				'ip_address' => $this->input->ip_address(),
			);

			$run = $this->Login_model->register($data);
			if ($run == 1) {

				$data = ['user_id' => 0, 'price_id' => '', 'product_id' => '', 'points' => '', 'game_id' => '', 'jems' => '', 'amount' => '', 'new_points' => '', 'status' => 'registration'];

				$this->logs($data);

				$this->session->set_flashdata('messageadd', 'Registered Successfully.');
				redirect(base_url('register'));
			} else {
				$this->session->set_flashdata('messageadd', 'Add error.');
				redirect(base_url('register'));
			}
		}
	}

	public function logs($data)
	{
		$data = array(
			'user_id' => $data['user_id'],
			'price_id' => $data['price_id'] ?? '',
			'product_id' => $data['product_id'] ?? '',
			'points' => $data['points'] ?? '',
			'new_points' => $data['new_points'] ?? '',
			'game_id' => $data['game_id'] ?? '',
			'jems' => $data['jems'] ?? '',
			'amount' => $data['amount'] ?? '',
			'status' => $data['status'] ?? '',
		);

		$this->db->insert('logs', $data);
	}
}