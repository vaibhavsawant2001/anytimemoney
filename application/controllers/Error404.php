<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Error404 extends CI_Controller {

    var $data = array();
	public function __construct(){
		parent::__construct();
		$this->load->model('Site_model');
    } 
    
	public function index()
	{
        $this->output->set_status_header('404');
        $data = [];
        $this->load->view('header',$data); 
        $this->load->view('404'); 
        $this->load->view('footer',$data); 
    }
}
?>