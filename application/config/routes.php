<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'Site';
$route['404_override'] = 'Error404';
$route['translate_uri_dashes'] = FALSE;


$route['register'] = "login/register";
$route['forget'] = "login/forgot";
$route['contact-us'] = "site/contactUs";
$route['shop'] = "site/products";
$route['shop-detail/(:any)'] = "site/product_detail/$1";
$route['games'] = "site/game";
$route['game-detail/(:any)'] = "site/game_detail/$1";
$route['favorite'] = "site/favorite";
$route['my_shopping'] = "site/my_shopping";
$route['transaction'] = "site/transaction";
$route['checkout/(:any)'] = "site/checkout/$1";