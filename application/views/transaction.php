<!-- Shop Start -->
<!-- Breadcrumb Start -->
<div class="container-fluid mt-4">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-decoration-none text-dark" href="<?= base_url() ?>">Home</a>
                <span class="breadcrumb-item active">Transaction</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->
<div class="container-fluid">
    <h5 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span
            class="bg-secondary pr-3">Transaction</span>
    </h5>
    <div class="row px-xl-5">
        <!-- Shop Product Start -->
        <div class="col-lg-12 col-md-8">
            <div class="row pb-3">
                <div class="table-responsive">
                    <table class="table table-bordered text-dark">
                        <thead>
                            <tr>
                                <th scope="col">Date</th>
                                <th scope="col">Product</th>
                                <th scope="col">Amount</th>
                                <th scope="col">Jem</th>
                                <th scope="col">Point</th>
                                <th scope="col">Price</th>
                                <th scope="col">Status</th>
                                <th scope="col">Comment</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php 
                            $users = $this->db->get_where('register', ['register_id' => $user_id])->result();
                            $transactions = $this->db->where_not_in('status', ('logout'))->get_where('logs', ['user_id' => $user_id])->result();
                            if (count($transactions) > 0) {
                                foreach ($transactions as $transaction) {
                                    $price = $this->db->get_where('games', ['id' => $transaction->price_id])->result();
                                    $product = $this->db->where_not_in('status', ('0'))->get_where('my_product', ['product_id' => $transaction->product_id])->result();
                                    $products = $this->db->where_not_in('status', ('0'))->get_where('products', ['id' => $transaction->product_id])->result();
                                    ?>
                                    <tr>
                                        <th scope="row">
                                            <?= date("d-m-Y", strtotime($transaction->created_at)) ?? '' ?>
                                        </th>
                                        <td class="text-capitalize">
                                            <?= $products[0]->product_name ?? '' ?>
                                        </td>
                                        <td>
                                            <?= $transaction->amount ?? '' ?>
                                        </td>
                                        <td>
                                            <?php if ($transaction->status == 'Approved By Admin') { ?>
                                                <?= $transaction->jems ?? '' ?>
                                            <?php }else{ ?> 0<?php }?>
                                        </td>
                                        <td>
                                            <?= $transaction->points ?? '' ?>
                                        </td>
                                        <td>
                                            <?= $price[0]->game_name ?? '' ?>
                                        </td>
                                        <td>
                                            <?= $transaction->status ?? '' ?>
                                        </td>
                                        <td class="text-capitalize">
                                            <?php if ($transaction->status !== 'Approval Request Sent') { ?>
                                                <?= $product[0]->comment ?? '' ?>
                                            <?php } ?>
                                        </td>

                                    </tr>
                                <?php }
                            } else { ?>
                            <tr>
                                <td colspan="7" class="text-center ">No Record Found</td>
                            </tr>
                            <?php } ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row ml-3">
        <div class="col-md-3">
            <table class="table table-bordered">
                <tr>
                    <td class="text-dark">Total Jems</td>
                    <td class="text-dark font-weight-bold" style="background-color:#c0c0c036"><?= $users[0]->total_jems??'0'?></td>
                </tr>
                <tr>
                    <td class="text-dark " style="background-color:#c0c0c036">Total Points</td>
                    <td class="text-dark font-weight-bold"><?= $users[0]->total_points ?? '0' ?></td>
                </tr>
            </table>
        </div>
    </div>

</div>