<?php
    $carousels = $this->db->order_by('id', 'desc')->get_where('banner', ['status' => '1'], 6)->result();
?>
<!-- Carousel Start -->
<div class=" mb-1 ">
    <div class="row_container px-xl-12" style="background-color:#3D464D">
        <div class="col-lg-12">
            <div id="header-carousel" class="carousel slide carousel-fade mb-0 mb-lg-0" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#header-carousel" data-slide-to="0" class="active"></li>
                    <li data-target="#header-carousel" data-slide-to="1"></li>
                    <li data-target="#header-carousel" data-slide-to="2"></li>
                </ol>
                <div class="carousel-inner">
                    <?php  foreach ($carousels as $carousel) { ?>
                    <div class="carousel-item position-relative <?= ($carousels[0]->id == $carousel->id) ? 'active' : '' ?>" style="height: 430px;">
                        <img class="position-absolute w-100 h-100" src="<?= base_url() ?>uploads/banner/<?= $carousel->image?>"
                            style="object-fit: cover;">
                        <div class="carousel-caption d-flex flex-column align-items-center justify-content-center">

                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

    <div>
        <ul class="nav nav-tabs bg-dark" id="myTab" role="tablist">
            <?php foreach ($categories as $category) { ?>
            <li class="nav-item" onclick="selectCategory('<?= $category['id'] ?>')">
                <a class="nav-link <?= $categories[0]['id'] == $category['id'] ? 'active' : '' ?>" id="home-tab"
                    data-toggle="tab" href="#home" role="tab" aria-controls="<?= $category['category'] ?>"
                    aria-selected="true"><?= $category['category'] ?></a>
            </li>
            <?php } ?>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="category_1" role="tabpanel" aria-labelledby="home-tab">
                <!-- Start -->
                <div class="container-fluid pt-5 pb-3">
                    <div class="row px-xl-5">
                        <?php $products_category = $this->db->order_by('id', 'desc')->get_where('products', ['category_id' => $categories[0]['id'], 'status' => '1'], 6)->result();
                        $prod_countg = count($products_category); ?>
                        <input type="hidden" readonly id="categorys_1" value="<?= $prod_countg ?? 0 ?>">
                        <?php
                        if ($prod_countg > 0) {
                            foreach ($products_category as $product) {
                                $user_id = (!empty($this->session->userdata('register_id'))) ? $this->session->userdata('register_id') : '';
                                $my_product = $this->db->get_where('my_product', ['product_id' => $product->id, 'user_id' => $user_id, 'status' => '1'], 3)->result();
                                $my_product_count = count($my_product);
                                ?>
                        <div class="col-lg-2 col-md-4 col-sm-6 pb-1">
                            <div class="product-item bg-light mb-4" style="height:480px">
                                <div class="product-img position-relative overflow-hidden" style="height:302px">
                                    <img class="img-fluid w-100" src="<?= base_url() ?>uploads/img/<?= $product->img ?>"
                                        alt="">
                                </div>
                                <div class="text-center py-4">
                                    <a class="h6 text-decoration-none text-truncate"
                                        href="<?= base_url('shop-detail/') ?><?= $product->id ?? '' ?>"><?= $product->product_name ?? '' ?></a>
                                    <div class="d-flex align-items-center justify-content-center mt-2">
                                        <h5><i class="fas fa-rupee-sign ml-1"></i>
                                            <?= $product->amount ?? '' ?>
                                        </h5>
                                        <h6 class="text-muted ml-2"><i class="fas fa-thin fa-gem ml-1 text-primary"></i>
                                            <?= $product->jems ?? '' ?>
                                        </h6>
                                    </div>
                                    <?php if (!empty($this->session->userdata('register_id'))) { ?>
                                    <a href="<?= base_url('checkout/') ?><?= $product->id ?? '' ?>">
                                        <div class="mb-4 pt-2">
                                            <button class="btn btn-primary px-3">
                                                <?= ($my_product_count > 0) ? 'Redeem Now' : 'Buy Now' ?>
                                            </button>
                                        </div>
                                    </a>
                                    <?php } else { ?>
                                    <div class=" mb-4 pt-2">
                                        <button class="btn btn-primary px-3 disabled" data-toggle="modal"
                                            data-target="#myModal">Buy
                                            Now</button>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php }
                        } else { ?>
                        <div class="col-lg-12 col-md-4 col-sm-6 pb-1">
                            <div class="cat-item font-weight-bold text-center p-3 mb-4">No price available</div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <?php if ($prod_countg > 5) { ?>
                <form action="<?= base_url('shop'); ?>" id="form_category" method="get">
                    <input type="hidden" id="category_filter" readonly value="1" name="category_id[]">
                    <center><button type="submit" class="btn btn-primary" style="margin-bottom: 15px;">More</button></a>
                    </center>
                </form>
                <?php } ?>
            </div>
            <!-- end -->
            <div class="tab-pane fade" id="category_2" role="tabpanel" aria-labelledby="profile-tab">
                <!-- Start -->
                <div class="container-fluid pt-5 pb-3">
                    <div class="row px-xl-5">
                        <?php $products_category = $this->db->order_by('id', 'desc')->get_where('products', ['category_id' => $categories[1]['id'], 'status' => '1'], 6)->result();
                        $prod_count = count($products_category); ?>
                        <input type="hidden" readonly id="categorys_2" value="<?= $prod_count ?? 0 ?>">
                        <?php
                        if ($prod_count > 0) {
                            foreach ($products_category as $product) {
                                $user_id = (!empty($this->session->userdata('register_id'))) ? $this->session->userdata('register_id') : '';
                                $my_product = $this->db->get_where('my_product', ['product_id' => $product->id, 'user_id' => $user_id, 'status' => '1'], 3)->result();
                                $my_product_count = count($my_product);
                                ?>
                        <div class="col-lg-2 col-md-4 col-sm-6 pb-1">
                            <div class="product-item bg-light mb-4" style="height:480px">
                                <div class="product-img position-relative overflow-hidden" style="height:302px">
                                    <img class="img-fluid w-100" src="<?= base_url() ?>uploads/img/<?= $product->img ?>"
                                        alt="">
                                </div>
                                <div class="text-center py-4">
                                    <a class="h6 text-decoration-none text-truncate"
                                        href="<?= base_url('shop-detail/') ?><?= $product->id ?? '' ?>"><?= $product->product_name ?? '' ?></a>
                                    <div class="d-flex align-items-center justify-content-center mt-2">
                                        <h5>
                                            <?= $product->amount ?? '' ?><i class="fas fa-rupee-sign ml-1"></i>
                                        </h5>
                                        <h6 class="text-muted ml-2">
                                            <?= $product->jems ?? '' ?><i
                                                class="fas fa-thin fa-gem ml-1 text-primary"></i>
                                        </h6>
                                    </div>
                                    <?php if (!empty($this->session->userdata('register_id'))) { ?>
                                    <a href="<?= base_url('checkout/') ?><?= $product->id ?? '' ?>">
                                        <div class="mb-4 pt-2">
                                            <button class="btn btn-primary px-3">
                                                <?= ($my_product_count > 0) ? 'Redeem Now' : 'Buy Now' ?>
                                            </button>
                                        </div>
                                    </a>
                                    <?php } else { ?>
                                    <div class=" mb-4 pt-2">
                                        <button class="btn btn-primary px-3 disabled" data-toggle="modal"
                                            data-target="#myModal">Buy
                                            Now</button>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php }
                        } else { ?>
                        <div class="col-lg-12 col-md-4 col-sm-6 pb-1">
                            <div class="cat-item font-weight-bold text-center p-3 mb-4">No price available</div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <form action="<?= base_url('shop'); ?>" id="form_categoryg" method="get">
                    <input type="hidden" id="category_filter" readonly value="1" name="category_id[]">
                    <center><button type="submit" class="btn btn-primary" style="margin-bottom: 15px;">More</button></a>
                    </center>
                </form>
                <!-- end -->
            </div>
            <div class="tab-pane fade" id="category_3" role="tabpanel" aria-labelledby="contact-tab">
                <div class="container-fluid pt-5 pb-3">
                    <div class="row px-xl-5">
                        <?php $products_category = $this->db->order_by('id', 'desc')->get_where('products', ['category_id' => $categories[2]['id'], 'status' => '1'], 6)->result();
                        $prod_count = count($products_category); ?>
                        <input type="hidden" readonly id="categorys_3" value="<?= $prod_count ?? 0 ?>">
                        <?php
                        if ($prod_count > 0) {
                            foreach ($products_category as $product) {
                                $user_id = (!empty($this->session->userdata('register_id'))) ? $this->session->userdata('register_id') : '';
                                $my_product = $this->db->get_where('my_product', ['product_id' => $product->id, 'user_id' => $user_id, 'status' => '1'], 3)->result();
                                $my_product_count = count($my_product);
                                ?>
                        <div class="col-lg-2 col-md-4 col-sm-6 pb-1">
                            <div class="product-item bg-light mb-4" style="height:480px">
                                <div class="product-img position-relative overflow-hidden" style="height:302px">
                                    <img class="img-fluid w-100" src="<?= base_url() ?>uploads/img/<?= $product->img ?>"
                                        alt="">
                                </div>
                                <div class="text-center py-4">
                                    <a class="h6 text-decoration-none text-truncate"
                                        href="<?= base_url('shop-detail/') ?><?= $product->id ?? '' ?>"><?= $product->product_name ?? '' ?></a>
                                    <div class="d-flex align-items-center justify-content-center mt-2">
                                        <h5>
                                            <?= $product->amount ?? '' ?><i class="fas fa-rupee-sign ml-1"></i>
                                        </h5>
                                        <h6 class="text-muted ml-2">
                                            <?= $product->jems ?? '' ?><i
                                                class="fas fa-thin fa-gem ml-1 text-primary"></i>
                                        </h6>
                                    </div>
                                    <?php if (!empty($this->session->userdata('register_id'))) { ?>
                                    <a href="<?= base_url('checkout/') ?><?= $product->id ?? '' ?>">
                                        <div class="mb-4 pt-2">
                                            <button class="btn btn-primary px-3">
                                                <?= ($my_product_count > 0) ? 'Redeem Now' : 'Buy Now' ?>
                                            </button>
                                        </div>
                                    </a>
                                    <?php } else { ?>
                                    <div class=" mb-4 pt-2">
                                        <button class="btn btn-primary px-3 disabled" data-toggle="modal"
                                            data-target="#myModal">Buy
                                            Now</button>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php }
                        } else { ?>
                        <div class="col-lg-12 col-md-4 col-sm-6 pb-1">
                            <div class="cat-item font-weight-bold text-center p-3 mb-4">No price available</div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
                <form action="<?= base_url('shop'); ?>" id="form_categorygg" method="get">
                    <input type="hidden" id="category_filter" readonly value="1" name="category_id[]">
                    <center><button type="submit" class="btn btn-primary" style="margin-bottom: 15px;">More</button></a>
                    </center>
                </form>
            </div>
        </div>

    </div>

    <!-- Carousel End -->

    <!-- Featured End -->

    <!-- Categories Start -->

</div>
<div class="container-fluid pt-3">

    <!-- <h2 class="mx-xl-4 mb-4"><span class="bg-secondary pr-3">Categories</span></h2> -->
    <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3">Game
            Price</span></h2>
    <div class="row px-xl-0 pb-0" style="padding: 0%;">

        <div class="col-lg-4 col-md-4 col-sm-6 pb-0">

            <div class="cat-item d-flex align-items-center mb-4"
                style="background: linear-gradient(90deg, rgba(255,0,0,1) 0%, rgba(254,48,28) 76%, rgb(224, 48, 48) 100%);">
                <h4 style="color: white;
                  text-align: center;
                  WIDTH: 100%;
                  MARGIN-TOP: 5PX;
                  FONT-FAMILY: math; ">Below-20000</h4>
            </div>
            <div>
                <?php $games = $this->db->order_by('id', 'desc')->where('amount >=', 1)->where('amount <=', 20000)->get_where('games', ['status' => '1'], 3)->result();
                $game_count = count($games);
                foreach ($games as $game) {
                    ?>
                <a class="text-decoration-none" href="<?= base_url('game-detail/') ?><?= $game->id ?? '' ?>">
                    <div class="cat-item d-flex align-items-center mb-4">
                        <div class="overflow-hidden" style="width: 100px; height: 100px;">
                            <img class="img-fluid" src="<?= base_url() ?>uploads/img/<?= $game->img ?? '' ?>" alt="">
                        </div>
                        <div class="flex-fill pl-3">
                            <h6>
                                <?= $game->game_name ?? '' ?>
                            </h6>
                            <small class="text-body">
                                <?= $game->points ?? '' ?> <i class="fas fa-thin fa-star ml-1 text-primary"></i>
                            </small>
                        </div>
                    </div>
                </a>
                <?php } ?>
                <?php if (!empty($games)) {
                    if ($game_count > 2) { ?>
                <form action="<?= base_url('games'); ?>" method="get">
                    <input type="hidden" id="category_filter" readonly value="20000" name="game_id">
                    <center><button type="submit" type="button" class="btn btn-primary"
                            style="margin-bottom: 15px;">More</button>
                    </center>
                </form>
                <?php }
                } else { ?>
                <div class="cat-item font-weight-bold text-center p-3 mb-4">No price available</div>
                <?php } ?>
            </div>
        </div>

        <div class="col-lg-4 col-md-4 col-sm-6 pb-1">

            <div class="cat-item d-flex align-items-center mb-4"
                style="background: linear-gradient(90deg, rgba(2,0,36,1) 0%, rgba(9,9,121,1) 35%, rgba(0,109,131,1) 100%);">
                <h4 style="color: white;
                  text-align: center;
                  WIDTH: 100%;
                  MARGIN-TOP: 5PX;
                  FONT-FAMILY: math; ">20000-40000</h4>

            </div>
            <div>
                <?php $games = $this->db->order_by('id', 'desc')->where('amount >=', 20001)->where('amount <=', 40000)->get_where('games', ['status' => '1'], 3)->result();
                $game_count = count($games);
                foreach ($games as $game) {
                    ?>
                <a class="text-decoration-none" href="<?= base_url('game-detail/') ?><?= $game->id ?? '' ?>">
                    <div class="cat-item d-flex align-items-center mb-4">
                        <div class="overflow-hidden" style="width: 100px; height: 100px;">
                            <img class="img-fluid" src="<?= base_url() ?>uploads/img/<?= $game->img ?? '' ?>" alt="">
                        </div>
                        <div class="flex-fill pl-3">
                            <h6>
                                <?= $game->game_name ?? '' ?>
                            </h6>
                            <small class="text-body">
                                <?= $game->points ?? '' ?> <i class="fas fa-thin fa-star ml-1 text-primary"></i>
                            </small>
                        </div>
                    </div>
                </a>
                <?php } ?>
                <?php if (!empty($games)) {
                    if ($game_count > 2) { ?>
                <form action="<?= base_url('games'); ?>" method="get">
                    <input type="hidden" id="category_filter" readonly value="40000" name="game_id">
                    <center><button type="submit" class="btn btn-primary" style="margin-bottom: 15px;">More</button>
                    </center>
                </form>
                <?php }
                } else { ?>
                <div class="cat-item font-weight-bold text-center p-3 mb-4">No price available</div>
                <?php } ?>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 pb-1">
            <div class="cat-item d-flex align-items-center mb-4"
                style="background: linear-gradient(90deg, rgb(214, 205, 82) 0%, rgb(218, 203, 75) 83%, rgb(201, 187, 34) 100%);">
                <h4 style="color: white;
                  text-align: center;
                  WIDTH: 100%;
                  MARGIN-TOP: 5PX;
                  FONT-FAMILY: math; ">40000-Above</h4>
            </div>
            <div>
                <?php $games = $this->db->order_by('id', 'desc')->where('amount >=', 40001)->get_where('games', ['status' => '1'], 3)->result();
                $game_count = count($games);
                foreach ($games as $game) {
                    ?>
                <a class="text-decoration-none" href="<?= base_url('game-detail/') ?><?= $game->id ?? '' ?>">
                    <div class="cat-item d-flex align-items-center mb-4">
                        <div class="overflow-hidden" style="width: 100px; height: 100px;">
                            <img class="img-fluid" src="<?= base_url() ?>uploads/img/<?= $game->img ?? '' ?>" alt="">
                        </div>
                        <div class="flex-fill pl-3">
                            <h6>
                                <?= $game->game_name ?? '' ?>
                            </h6>

                            <i class="fas fa-thin fa-star ml-1 text-primary"></i> <small class="text-body">
                                <?= $game->points ?? '' ?> <i class="fas fa-thin fa-star ml-1 text-primary"></i>
                            </small>
                        </div>
                    </div>
                </a>
                <?php } ?>
                <?php if (!empty($games)) {
                    if ($game_count > 2) { ?>
                <form action="<?= base_url('games'); ?>" method="get">
                    <input type="hidden" id="category_filter" readonly value="10000000" name="game_id">
                    <center><button type="submit" class="btn btn-primary" style="margin-bottom: 15px;">More</button>
                    </center>
                </form>
                <?php }
                } else { ?>
                <div class="cat-item font-weight-bold text-center p-3 mb-4">No price available</div>
                <?php } ?>
            </div>
        </div>
    </div>

    <!-- Products Start -->

    <!-- Products End -->

    <!-- Offer Start  -->
    <div class="container-fluid pt-5 pb-3">
        <div class="row px-xl-5">
            <div class="col-md-6">
                <div class="product-offer mb-30" style="height: 300px;">
                    <img class="img-fluid" src="<?= base_url() ?>uploads/img/game-6.jpg" alt="">
                    <div class="offer-text">
                        <h6 class="text-white text-uppercase">Save 20%</h6>
                        <h3 class="text-white mb-3">Special Offer</h3>
                        <a href="" class="btn btn-primary">Shop Now</a>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="product-offer mb-30" style="height: 300px;">
                    <img class="img-fluid" src="<?= base_url() ?>uploads/img/game-5.jpg" alt="">
                    <div class="offer-text">
                        <h6 class="text-white text-uppercase">Save 20%</h6>
                        <h3 class="text-white mb-3">Special Offer</h3>
                        <a href="" class="btn btn-primary">Shop Now</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Offer End-->

    <!-- Products Start -->

    <!-- Products End -->
    <!-- Vendor Start -->
    <div class="container-fluid py-5">
        <div class="row px-xl-5">
            <div class="col">
                <div class="owl-carousel vendor-carousel">
                    <div class="bg-light p-4">
                        <img src="<?= base_url() ?>uploads/img/vendor-1.jpg" alt="">
                    </div>
                    <div class="bg-light p-4">
                        <img src="<?= base_url() ?>uploads/img/vendor-2.jpg" alt="">
                    </div>
                    <div class="bg-light p-4">
                        <img src="<?= base_url() ?>uploads/img/vendor-3.jpg" alt="">
                    </div>
                    <div class="bg-light p-4">
                        <img src="<?= base_url() ?>uploads/img/vendor-4.jpg" alt="">
                    </div>
                    <div class="bg-light p-4">
                        <img src="<?= base_url() ?>uploads/img/vendor-5.jpg" alt="">
                    </div>
                    <div class="bg-light p-4">
                        <img src="<?= base_url() ?>uploads/img/vendor-6.jpg" alt="">
                    </div>
                    <div class="bg-light p-4">
                        <img src="<?= base_url() ?>uploads/img/vendor-7.jpg" alt="">
                    </div>
                    <div class="bg-light p-4">
                        <img src="<?= base_url() ?>uploads/img/vendor-8.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Vendor End -->
    <script>
    function selectCategory(id) {
        if (id == 1) {
            $("#category_2").removeClass('show active');
            $("#category_3").removeClass('show active');
            $("#category_1").addClass('show active');
            $("#category_filter").val(id);
            var count = $("#categorys_1").val();
            if (count > 5) {
                $("#form_category").show();
            } else {
                $("#form_category").hide();
            }
        }

        if (id == 2) {
            $("#category_1").removeClass('show active');
            $("#category_3").removeClass('show active');
            $("#category_2").addClass('show active');
            $("#category_filter").val(id);
            var count = $("#categorys_2").val();
            if (count > 5) {
                $("#form_categoryg").show();
            } else {
                $("#form_categoryg").hide();
            }
        }

        if (id == 3) {
            $("#category_1").removeClass('show active');
            $("#category_2").removeClass('show active');
            $("#category_3").addClass('show active');
            $("#category_filter").val(id);
            var count = $("#categorys_3").val();
            if (count > 5) {
                $("#form_categorygg").show();
            } else {
                $("#form_categorygg").hide();
            }
        }


    }
    </script>