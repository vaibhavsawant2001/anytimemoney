<?php if (!empty($this->session->userdata('register_id'))) {
    redirect(base_url());
} else { ?>
    <!-- Breadcrumb Start -->
    <div class="container-fluid mt-4">
        <div class="row px-xl-5">
            <div class="col-12">
                <nav class="breadcrumb bg-light mb-30">
                    <a class="breadcrumb-item text-decoration-none text-dark" href="<?= base_url() ?>">Home</a>
                    <span class="breadcrumb-item active">Register</span>
                </nav>
            </div>
        </div>
    </div>
    <!-- Breadcrumb End -->

    <!-- Checkout Start -->
    <div class="container-fluid">
        <div class="row px-xl-5">
            <div class="col-lg-12">
                <h5 class="section-title position-relative text-uppercase mb-3"><span
                        class="bg-secondary pr-3">Registration</span></h5>
                <div class="bg-light p-30 mb-5">
                    <form action="<?= base_url('Login/registrations'); ?>" method="post">
                        <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                            value="<?php echo $this->security->get_csrf_hash(); ?>">
                        <div class="bg-light p-30">
                            <div class="row">
                                <?php if ($this->session->flashdata('messageadd') !== null) { ?>
                                    <div class="col-md-12">
                                        <div class="alert alert-success">
                                            <strong>
                                                <?= $this->session->flashdata('messageadd'); ?>
                                            </strong>
                                        </div>
                                    </div>
                                <?php } ?>
                                <?php if ($this->session->flashdata('danger') !== null) { ?>
                                    <div class="col-md-12">
                                        <div class="alert alert-danger">
                                            <strong>
                                                <?= $this->session->flashdata('danger'); ?>
                                            </strong>
                                        </div>
                                    </div>
                                <?php } ?>
                                <div class="col-md-6 form-group">
                                    <label>Full Name *</label>
                                    <input class="form-control" name="name" type="text" placeholder="EnterFull Name"
                                        required>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>E-mail *</label>
                                    <input class="form-control" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" name="email" type="email" placeholder="Enter E-mail"
                                        required>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Mobile No *</label>
                                    <input pattern="[1-9]{1}[0-9]{9}" class="form-control" name="phone" type="tel"
                                        placeholder="Enter Mobile No" required>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Address *</label>
                                    <textarea class="form-control" name="city" type="text" required></textarea>
                                </div>
                                <div class="col-md-6 form-group">
                                    <label>Password *</label>
                                    <input required class="form-control" type="password" name="password"
                                        placeholder="Enter Password" />
                                </div>
                                <div class="col-md-12 form-group mt-2">
                                    <button type="submit" class="btn  btn-primary font-weight-bold px-4 py-2">Register</button>
                                </div>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <!-- Checkout End -->
    </div>
<?php } ?>