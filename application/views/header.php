<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <?php if (!empty(@$seo['title']) || !empty(@$seo['metatag'])) { ?>
        <title>
            <?= @$seo['page_title'] . ' | Any Time Money'; ?>
        </title>
        <?= @$seo['metatag']; ?>
    <?php } ?>

    <!-- Favicon -->
    <link href="<?= base_url() ?>uploads/img/favicon.ico" rel="icon">

    <!-- Google Web Fonts -->
    <!-- <link rel="preconnect" href=""> -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">

    <!-- Libraries Stylesheet -->
    <link href="<?= base_url() ?>lib/animate/animate.min.css" rel="stylesheet">
    <link href="<?= base_url() ?>lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

    <!-- Customized Bootstrap Stylesheet -->
    <link href="<?= base_url() ?>css/style.css" rel="stylesheet">
    <style>
        html,
        body {
            height: 100%;
        }
    </style>
</head>

<body>
    <!-- Topbar Start  -->

    <div class="col-lg-0 text-center text-lg-right b-block d-md-none" style="background-color: black; color: white;">
        <div class="d-inline-flex align-items-right">

            <?php if (!empty($this->session->userdata('register_id'))) {
                $fav = $this->db->get_where('fav_list', ['login_id' => $this->session->userdata('register_id'), 'status' => '1'])->result();
                $my_product = $this->db->get_where('my_product', ['user_id' => $this->session->userdata('register_id'), 'status' => '1'])->result();
                $jems = $this->db->select('total_jems')->get_where('register', ['status' => 1, 'register_id' => $this->session->userdata('register_id')])->result();
                $points = $this->db->select('total_points')->get_where('register', ['status' => 1, 'register_id' => $this->session->userdata('register_id')])->result();
                ?>
                <a href="javascript:void(0);" class="btn px-0" style="color: white;"></a>
                <a href="javascript:void(0);" class="btn px-0">
                    <span class="badge text-secondary border border-secondary rounded-circle"
                        style="padding-bottom: 2px;"><?= $jems[0]->total_jems ?? 0 ?></span>
                    <i class="fas fa-thin fa-gem text-primary"></i>
                </a>
                <a href="javascript:void(0);" class="btn px-0" style="color: white;"></a>
                <a href="javascript:void(0);" class="btn px-0">
                    <span class="badge text-secondary border border-secondary rounded-circle" style="padding-bottom: 2px;">
                        <?= $points[0]->total_points ?? 0 ?>
                    </span>
                    <i class="fas fa-thin fa-star text-primary"></i>

                </a>
                <a href="<?= base_url('favorite') ?>"
                    class="btn px-0 <?= ($this->uri->segment(1) == 'favorite') ? 'text-warning' : ''; ?>"
                    style="color: white;"></a>
                <a href="<?= base_url('favorite') ?>" class="btn px-0">
                    <span class="badge text-secondary  border border-secondary rounded-circle" style="padding-bottom: 2px;">
                        <?= count($fav) ?? 0 ?>
                    </span>
                    <i class="fas fa-heart text-primary"></i>
                </a>
                <a href="<?= base_url('my_shopping') ?>"
                    class="btn px-0 <?= ($this->uri->segment(1) == 'my_shopping') ? 'text-warning' : ''; ?>"
                    style="color: white;">
                </a>
                <a href="<?= base_url('my_shopping') ?>" class="btn px-0">
                    <span class="badge text-secondary  border border-secondary rounded-circle" style="padding-bottom: 2px;">
                        <?= count($my_product) ?? 0 ?>
                    </span>
                    <i class="fas fa-shopping-cart text-primary"></i>
                </a>
            <?php } else { ?>
                <!-- <a href="<?= base_url(); ?>" class="btn px-0" style="color: white;">Home</a>
                <a href="<?= base_url(); ?>" class="btn px-0" style="color: white;">Register</a>
                <a href="<?= base_url(); ?>" class="btn px-0" style="color: white;">Login</a> -->
            <?php } ?>
        </div>
    </div>

    <!-- Topbar End -->

    <!-- Navbar Start -->
    <div class="container-fluid bg-dark mb-0">
        <div class="row px-xl-0">
            <div class="col-lg-3 d-none d-lg-block">
                <img src="<?= base_url() ?>uploads/img/logo_W.png" alt="" style="width: 200px;margin-top: 5px;">
            </div>
            <?php if (!empty($this->session->userdata('register_id'))) { ?>
                <div class="col-lg-9">
                    <nav class="navbar navbar-expand-lg bg-dark navbar-dark py-2 py-lg-0 px-0">
                        <a href="" class="text-decoration-none d-block d-lg-none">
                            <img src="<?= base_url() ?>uploads/img/logo_W.png" alt="" style="width: 200px;">

                        </a>
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                            <?php if (!empty($this->session->userdata('register_id'))) { ?>
                                <div class="navbar-nav mr-auto py-0">
                                    <a href="<?= base_url(); ?>"
                                        class="nav-item nav-link <?= ($this->uri->segment(1) == '' || $this->uri->segment(1) == 'home') ? 'text-warning' : ''; ?>">Home</a>
                                    <a href="<?= base_url('shop'); ?>"
                                        class="nav-item nav-link <?= ($this->uri->segment(1) == 'shop' || $this->uri->segment(1) == 'shop-detail') ? 'text-warning' : ''; ?>">Shop</a>
                                    <a href="<?= base_url('games'); ?>"
                                        class="nav-item nav-link <?= ($this->uri->segment(1) == 'games' || $this->uri->segment(1) == 'game-detail') ? 'text-warning' : ''; ?>">Game</a>
                                    <a href="<?= base_url('contact-us'); ?>"
                                        class="nav-item nav-link <?= ($this->uri->segment(1) == 'contact-us') ? 'text-warning' : ''; ?>">Contact</a>
                                    <a href="<?= base_url('transaction'); ?>"
                                        class="nav-item nav-link <?= ($this->uri->segment(1) == 'transaction') ? 'text-warning' : ''; ?>">Transaction</a>
                                    <a href="<?= base_url('Login/logout'); ?>" class="nav-item nav-link">Logout</a>
                                </div>
                            <?php } ?>


                            <?php if (!empty($this->session->userdata('register_id'))) {
                                $fav = $this->db->get_where('fav_list', ['login_id' => $this->session->userdata('register_id'), 'status' => '1'])->result();
                                $my_product = $this->db->get_where('my_product', ['user_id' => $this->session->userdata('register_id'), 'status' => '1'])->result();
                                $jems = $this->db->select('total_jems')->get_where('register', ['status' => 1, 'register_id' => $this->session->userdata('register_id')])->result();
                                $points = $this->db->select('total_points')->get_where('register', ['status' => 1, 'register_id' => $this->session->userdata('register_id')])->result();
                                ?>
                                <div class="navbar-nav ml-auto py-0 d-none d-lg-block">
                                    <a href="javascript:void(0);" class="btn px-0" style="color: white;">Jems</a>
                                    <a href="javascript:void(0);" class="btn px-0">
                                        <i class="fas fa-thin fa-gem text-primary"></i>
                                        <span class="badge text-secondary border border-secondary rounded-circle"
                                            style="padding-bottom: 2px;"><?= $jems[0]->total_jems ?? 0 ?></span>
                                    </a>
                                    <a href="javascript:void(0);" class="btn px-0" style="color: white;">Point</a>
                                    <a href="javascript:void(0);" class="btn px-0">
                                        <i class="fas fa-thin fa-star text-primary"></i>
                                        <span class="badge text-secondary border border-secondary rounded-circle"
                                            style="padding-bottom: 2px;"><?= $points[0]->total_points ?? 0 ?></span>
                                    </a>
                                    <a href="<?= base_url('favorite') ?>"
                                        class="btn px-0 <?= ($this->uri->segment(1) == 'favorite') ? 'text-warning' : ''; ?>"
                                        style="color: white;">Favourite</a>
                                    <a href="<?= base_url('favorite') ?>" class="btn px-0">
                                        <i class="fas fa-heart text-primary"></i>
                                        <span class="badge text-secondary  border border-secondary rounded-circle"
                                            style="padding-bottom: 2px;"><?= count($fav) ?? 0 ?></span>
                                    </a>
                                    <a href="<?= base_url('my_shopping') ?>"
                                        class="btn px-0 <?= ($this->uri->segment(1) == 'my_shopping') ? 'text-warning' : ''; ?>"
                                        style="color: white;">My Shopping</a>
                                    <a href="<?= base_url('my_shopping') ?>" class="btn px-0">
                                        <i class="fas fa-shopping-cart text-primary"></i>
                                        <span class="badge text-secondary  border border-secondary rounded-circle"
                                            style="padding-bottom: 2px;"><?= count($my_product) ?? 0 ?></span>
                                    </a>
                                </div>
                            <?php } else { ?>
                                <div class="navbar-nav ml-auto  py-2 d-none d-lg-block">

                                    <a href="<?= base_url(); ?>"
                                        class="btn <?= ($this->uri->segment(1) == '' || $this->uri->segment(1) == 'home') ? 'text-warning' : ''; ?> px-2"
                                        style="color: white;">Home</a>
                                    <a href="<?= base_url('shop'); ?>"
                                        class="btn <?= ($this->uri->segment(1) == 'shop' || $this->uri->segment(1) == 'shop-detail') ? 'text-warning' : ''; ?> px-2"
                                        style="color: white;">Shop</a>
                                    <a href="<?= base_url('games'); ?>"
                                        class="btn <?= ($this->uri->segment(1) == 'games' || $this->uri->segment(1) == 'game-detail') ? 'text-warning' : ''; ?> px-2"
                                        style="color: white;">Game</a>
                                    <a href="<?= base_url('contact-us'); ?>"
                                        class="btn <?= ($this->uri->segment(1) == 'contact-us') ? 'text-warning' : ''; ?>  px-2"
                                        style="color: white;">Contact</a>
                                    <a href="<?= base_url('register'); ?>"
                                        class="btn <?= ($this->uri->segment(1) == 'register') ? 'text-warning' : ''; ?> px-2"
                                        style="color: white;">Register</a>
                                    <a href="<?= base_url('login'); ?>"
                                        class="btn <?= ($this->uri->segment(1) == 'login') ? 'text-warning' : ''; ?> px-2"
                                        style="color: white;">Login</a>
                                </div>
                            <?php } ?>
                        </div>
                    </nav>
                </div>
            <?php } else { ?>
                <div class="col-lg-9">
                    <nav class="navbar navbar-expand-lg bg-dark navbar-dark py-2 py-lg-0 px-0">
                        <a href="" class="text-decoration-none d-block d-lg-none">
                            <img src="<?= base_url() ?>uploads/img/logo_W.png" alt="" style="width: 200px;">

                        </a>
                        <button type="button" class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse justify-content-between" id="navbarCollapse">
                            <div class="navbar-nav mr-auto py-0">
                                <a href="<?= base_url(); ?>"
                                    class="nav-item nav-link <?= ($this->uri->segment(1) == '' || $this->uri->segment(1) == 'home') ? 'text-warning' : ''; ?>">Home</a>
                                <a href="<?= base_url('shop'); ?>"
                                    class="nav-item nav-link <?= ($this->uri->segment(1) == 'shop' || $this->uri->segment(1) == 'shop-detail') ? 'text-warning' : ''; ?>">Shop</a>
                                <a href="<?= base_url('games'); ?>"
                                    class="nav-item nav-link <?= ($this->uri->segment(1) == 'games' || $this->uri->segment(1) == 'game-detail') ? 'text-warning' : ''; ?>">Game</a>
                                <a href="<?= base_url('contact-us'); ?>"
                                    class="nav-item nav-link <?= ($this->uri->segment(1) == 'contact-us') ? 'text-warning' : ''; ?>">Contact</a>
                                <a href="<?= base_url('register'); ?>"
                                    class="nav-item nav-link <?= ($this->uri->segment(1) == 'register') ? 'text-warning' : ''; ?>"
                                    style="color: white;">Register</a>
                                <a href="<?= base_url('login'); ?>"
                                    class="nav-item nav-link <?= ($this->uri->segment(1) == 'login') ? 'text-warning' : ''; ?>"
                                    style="color: white;">Login</a>
                            </div>



                            <div class="navbar-nav ml-auto  py-2 d-none d-lg-block">

                                <a href="<?= base_url(); ?>"
                                    class="btn <?= ($this->uri->segment(1) == '' || $this->uri->segment(1) == 'home') ? 'text-warning' : ''; ?> px-2"
                                    style="color: white;">Home</a>
                                <a href="<?= base_url('shop'); ?>"
                                    class="btn <?= ($this->uri->segment(1) == 'shop' || $this->uri->segment(1) == 'shop-detail') ? 'text-warning' : ''; ?> px-2"
                                    style="color: white;">Shop</a>
                                <a href="<?= base_url('games'); ?>"
                                    class="btn <?= ($this->uri->segment(1) == 'games' || $this->uri->segment(1) == 'game-detail') ? 'text-warning' : ''; ?> px-2"
                                    style="color: white;">Game</a>
                                <a href="<?= base_url('contact-us'); ?>"
                                    class="btn <?= ($this->uri->segment(1) == 'contact-us') ? 'text-warning' : ''; ?>  px-2"
                                    style="color: white;">Contact</a>
                                <a href="<?= base_url('register'); ?>"
                                    class="btn <?= ($this->uri->segment(1) == 'register') ? 'text-warning' : ''; ?> px-2"
                                    style="color: white;">Register</a>
                                <a href="<?= base_url('login'); ?>"
                                    class="btn <?= ($this->uri->segment(1) == 'login') ? 'text-warning' : ''; ?> px-2"
                                    style="color: white;">Login</a>
                            </div>
                        </div>
                    </nav>
                </div>
            <?php } ?>
        </div>
    </div>
    <!-- Navbar End -->