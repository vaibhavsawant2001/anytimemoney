<?php if (!empty($this->session->userdata('register_id'))) {
    redirect(base_url());
} else { ?>
<!-- Breadcrumb Start -->
<div class="container-fluid mt-4">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-decoration-none text-dark" href="<?= base_url() ?>">Home</a>
                <span class="breadcrumb-item active">Login</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->

<!-- Checkout Start -->
<div class="container-fluid">
   <div class="row  px-xl-5 d-flex align-items-center justify-content-center">
        <div class="col-lg-12">
            <h5 class="section-title position-relative text-uppercase mb-3"><span class="bg-secondary pr-3">Login</span>
            </h5>
        </div>
        <div class="col-lg-8">
            <div class="bg-light p-30 mb-5">
                <form action="<?= base_url('Login/user_login'); ?>" method="post">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                        value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <div class="bg-light p-30">
                        <div class="row">
                            <?php if ($this->session->flashdata('incorrect') !== null) { ?>
                            <div class="col-md-12">
                                <div class="alert alert-danger">
                                    <strong>
                                        <?= $this->session->flashdata('incorrect'); ?>
                                    </strong>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="col-md-12 form-group">
                                <label>E-mail *</label>
                                <input class="form-control" name="email" type="email"
                                    pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="Enter E-mail"
                                    required>
                            </div>
                            <div class="col-md-12 form-group">
                                <label>Password *</label>
                                <input required class="form-control" type="password" name="password"
                                    placeholder="Enter Password" />
                            </div>
                            <div class="col-md-12">
                                <a class="text-info text-decoration-none" href="<?= base_url('forget'); ?>">forget Password ?</a>
                            </div>
                            <div class="col-md-12 mt-2">
                                <button type="submit" class="btn  btn-primary font-weight-bold px-4 py-2">Login</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <!-- Checkout End -->
</div>
<?php } ?>