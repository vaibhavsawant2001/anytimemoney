
			<section class="page_title ls s-py-50 corner-title ls invise overflow-visible" style="margin-top: 50px;">
				<div class="container">
					<div class="row">
						<div class="col-md-12 text-center">
							<h1>404</h1>
							<ol class="breadcrumb">
								<li class="breadcrumb-item">
                                    <a href="<?= base_url(); ?>">Home</a>
								</li>
								<li class="breadcrumb-item active">
									404
								</li>
							</ol>
							<div class="divider-15 d-none d-xl-block"></div>
						</div>
					</div>
				</div>
			</section>


			<section class="ls s-pt-50 s-pb-100 error-404 not-found page_404">
				<div class="container">
					<div class="row">
						<div class="col-sm-12 text-center">

							<header class="page-header">
								<p>
									Oops, Page not found
								</p>
							</header>
							<!-- .page-header -->

							<div class="page-content">
								<div id="search-404" class="widget widget_search">

									<!-- <form role="search" method="get" class="search-form" action="https://webdesign-finder.com/">
										<label for="search-form-404">
											<span class="screen-reader-text">Search for:</span>
										</label>
										<input type="search" id="search-form-404" class="search-field text-center" placeholder="Search" value="" name="search">
										<button type="submit" class="search-submit">
											<span class="screen-reader-text">Search</span>
										</button>
									</form> -->
								</div>

								<p>
									<a href="<?= base_url(); ?>" class="btn btn-maincolor">Homepage</a>
								</p>
							</div>
							<!-- .page-content -->
						</div>
					</div>
				</div>
				<div class="d-none d-lg-block divider-120"></div>
			</section>