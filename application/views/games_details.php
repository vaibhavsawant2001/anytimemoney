<?php 
$user_id = (!empty($this->session->userdata('register_id'))) ? $this->session->userdata('register_id')  : '';
$my_game = $this->db->get_where('my_price', ['price_id'=> $product->id,'user_id'=> $user_id,'status' => '1'])->result();
$my_game_count = count($my_game);
?>
<!-- Shop Start -->
<!-- Breadcrumb Start -->
<div class="container-fluid mt-4">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-decoration-none text-dark" href="<?= base_url() ?>">Home</a>
                <a class="breadcrumb-item text-decoration-none text-dark" href="<?= base_url('games') ?>">Game</a>
                <span class="breadcrumb-item active">Game Details</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->
<!-- Shop Detail Start -->
<div class="container-fluid pb-5">
    <div class="row px-xl-5">
        <div class="col-lg-3 mb-30">
            <div id="product-carousel" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner bg-light">
                    <div class="carousel-item active">
                        <img class="w-100 h-50" src="<?= base_url(); ?>uploads/img/<?= $product->img??'' ?>"
                            alt="Image">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-9 h-auto mb-30">
            <div class="h-100 bg-light p-30">
                <?php if ($this->session->flashdata('error') !== null) { ?>
                <div class="alert alert-danger">
                    <strong>
                        <?= $this->session->flashdata('error'); ?>
                    </strong>
                </div>
                <?php } ?>

                <?php if ($this->session->flashdata('success') !== null) { ?>
                <div class="alert alert-primary">
                    <strong>
                        <?= $this->session->flashdata('success'); ?>
                    </strong>
                </div>
                <?php } ?>
                <h3>
                    <?= $product->game_name ?? '' ?>
                </h3>
                <h3 class="font-weight-semi-bold mb-4">
                    <i class="fas fa-rupee-sign ml-1"></i>
                    <?= $product->amount??'' ?>
                    <i class="fas fa-thin fa-star ml-1 text-primary"></i>
                    <?= $product->points ?? '' ?>
                </h3>
                <?php if (!empty($product->short_desc)) { ?>
                <p class="mb-4"><?=$product->short_desc?></p>
                <?php } ?>
                <?php if (!empty($this->session->userdata('register_id'))) { ?>
                <div class="d-flex align-items-center mb-4 pt-2">
                    <div class=" mb-4 pt-2">
                        <button class="btn btn-primary px-3" data-toggle="modal" data-target="#myModalPrice"
                            onclick="pop_up_price('<?= $product->id ?? '' ?>')"><?=($my_game_count > 0)? 'Reclaim': 'Claim'?></button>
                    </div>
                </div>
                <?php }else{ ?>
                <div class="d-flex align-items-center mb-4 pt-2">
                    <button class="btn btn-primary px-3 disabled" data-toggle="modal"
                        data-target="#myModal">Claim</button>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <?php if (!empty($product->long_desc)) { ?>
    <div class="row px-xl-5">
        <div class="col">
            <div class="bg-light p-30">
                <div class="nav nav-tabs mb-4">
                    <a class="nav-item nav-link text-dark active" data-toggle="tab" href="#tab-pane-1">Description</a>
                </div>
                <div class="tab-content">
                    <div class="tab-pane fade show active" id="tab-pane-1">
                        <h4 class="mb-3">Product Description</h4>
                        <?= $product->long_desc?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
<!-- Shop Detail End -->

<!-- Products Start -->
<div class="container-fluid py-5">
    <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3">You May Also
            Like</span></h2>
    <div class="row px-xl-5">
        <div class="col">
            <div class="owl-carousel related-carousel">
                <?php 
                    foreach($similar_products as $product){
                ?>
                <div class="product-item bg-light mb-4" style="height:610px">
                    <div class="product-img position-relative overflow-hidden" style="height:452px">
                        <img class="img-fluid w-100" src="<?= base_url() ?>uploads/img/<?= $product->img?>" alt="">
                    </div>
                    <div class="text-center py-4">
                        <a class="h6 text-decoration-none text-truncate"
                            href="<?= base_url('game-detail/') ?><?= $product->id ?? '' ?>"><?= $product->game_name??'' ?></a>
                        <div class="d-flex align-items-center justify-content-center mt-2">
                            <h5><?= $product->amount ?? '' ?><i class="fas fa-rupee-sign ml-1"></i></h5>
                            <h6 class="text-muted ml-2"><?= $product->points ?? '' ?><i
                                    class="fas fa-thin fa-star ml-1 text-primary"></i></h6>
                        </div>
                        <!-- <?php if (!empty($this->session->userdata('register_id'))) { ?>
                        <div class=" mb-4 pt-2">
                            <button class="btn btn-primary px-3" data-toggle="modal" data-target="#myModalPrice"
                                onclick="pop_up_price('<?= $product->id ?? '' ?>')">Claim</button>
                        </div>
                        <?php } else { ?>
                        <div class=" mb-4 pt-2">
                            <button class="btn btn-primary px-3 disabled" data-toggle="modal"
                                data-target="#myModal">Claim</button>
                        </div>
                        <?php } ?> -->
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>
<!-- Products End -->