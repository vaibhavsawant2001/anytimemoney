<!-- Breadcrumb Start -->
<div class="container-fluid mt-4">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-decoration-none text-dark" href="<?= base_url() ?>">Home</a>
                <a class="breadcrumb-item text-decoration-none text-dark" href="<?= base_url('shop') ?>">Shop</a>
                <a class="breadcrumb-item text-decoration-none text-dark"
                    href="<?= base_url('shop-detail/'.$product_id) ?>">Shop Details</a>
                <span class="breadcrumb-item active">Checkout</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->

<!-- Checkout Start -->
<div class="container-fluid">
    <div class="row  px-xl-5 d-flex align-items-center justify-content-center">
        <div class="col-lg-12">
            <h5 class="section-title position-relative text-uppercase mb-3"><span
                    class="bg-secondary pr-3">Checkout</span>
            </h5>
        </div>
        <div class="col-lg-8">
            <div class="bg-light p-30 mb-5">
                <form action="<?= base_url('Site/checkout_upload'); ?>" enctype="multipart/form-data" method="post">
                    <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                        value="<?php echo $this->security->get_csrf_hash(); ?>">
                    <div class="bg-light p-30">
                        <div class="row">
                            <?php if ($this->session->flashdata('error') !== null) { ?>
                            <div class="col-md-12">
                                <div class="alert alert-danger">
                                    <strong>
                                        <?= $this->session->flashdata('error'); ?>
                                    </strong>
                                </div>
                            </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('success') !== null) { ?>
                            <div class="col-md-12">
                                <div class="alert alert-danger">
                                    <strong>
                                        <?= $this->session->flashdata('success'); ?>
                                    </strong>
                                </div>
                            </div>
                            <?php } ?>
                            <div class="col-md-12 h5 bg-primary p-2 text-dark text-center form-group">Please upload your
                                payment recieve here</div>
                            <div class="col-md-12 form-group">
                                <label>File *</label>
                                <input type="hidden" name="product_id" value="<?=$product_id?>" readonly>
                                <input required class="form-control" type="file" name="image"
                                    placeholder="Enter Password" />
                            </div>

                            <div class="col-md-12 mt-2">
                                <button type="submit" class="btn  btn-primary font-weight-bold px-2 py-2">Upload
                                    Recieve</button>
                            </div>
                        </div>
                    </div>
                </form>
                <?php $inpox = $this->db->get_where('ck_editor_checkout', [], 1)->result();
                ?>
                 <div class="col-md-12 form-group"><?= $inpox[0]->message??''?></div>
            </div>
        </div>
    </div>
    <!-- Checkout End -->
</div>