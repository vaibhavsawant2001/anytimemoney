<!-- Shop Start -->
<!-- Breadcrumb Start -->
<div class="container-fluid mt-4">
    <div class="row px-xl-5">
        <div class="col-12">
            <nav class="breadcrumb bg-light mb-30">
                <a class="breadcrumb-item text-decoration-none text-dark" href="<?= base_url() ?>">Home</a>
                <span class="breadcrumb-item active">Game</span>
            </nav>
        </div>
    </div>
</div>
<!-- Breadcrumb End -->
<div class="container-fluid">
    <h5 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span class="bg-secondary pr-3">Game</span>
    </h5>
    <div class="row px-xl-5">
        <!-- Shop Sidebar Start -->
        <div class="col-lg-3 col-md-4">
            <!-- Price Start -->

            <div class="bg-light p-4 mb-30">
                <form action="<?= base_url('games')?>" method="get">
                    <label>All Price</label>
                    <div class="custom-control custom-radio d-flex align-items-center justify-content-between mb-3">
                        <input type="radio" onclick="checkout_val()"
                            <?=(!empty($_GET['game_id'])) ? ($_GET['game_id'] == 20000 ) ? 'checked' : '' : ''?>
                            class="custom-control-input" value="20000" name="game_id" id="price-2">
                        <label class="custom-control-label" for="price-2">Less than 20000</label>
                    </div>
                    <div class="custom-control custom-radio d-flex align-items-center justify-content-between mb-3">
                        <input type="radio" onclick="checkout_val()"
                            <?= (!empty($_GET['game_id'])) ? ($_GET['game_id'] == 40000) ? 'checked' : '' : '' ?>
                            class="custom-control-input" value="40000" name="game_id" id="price-3">
                        <label class="custom-control-label" for="price-3">20000 - 40000</label>
                    </div>
                    <div class="custom-control custom-radio d-flex align-items-center justify-content-between mb-3">
                        <input type="radio" onclick="checkout_val()"
                            <?= (!empty($_GET['game_id'])) ? ($_GET['game_id'] == 10000000) ? 'checked' : '' : '' ?>
                            class="custom-control-input" value="10000000" name="game_id" id="price-4">
                        <label class="custom-control-label" for="price-4">More than 40000</label>
                    </div>
                    <button type="submit" id="filters" name="filter" value="filter"
                        class="btn btn-disable btn-primary font-weight-bold px-3 py-1">Filter</button>
                </form>
            </div>
            <!-- Price End -->
        </div>
        <!-- Shop Sidebar End -->

        <!-- Shop Product Start -->
        <div class="col-lg-9 col-md-8">
            <div class="row pb-3">
                <?php 
                if(count($products_category) > 0){

                foreach($products_category as $product){ ?>
                <div class="col-lg-4 col-md-6 col-sm-6 pb-1">
                    <div class="product-item bg-light mb-4" style="height:620px">
                        <div class="product-img position-relative  overflow-hidden" style="height:452px">
                            <img class="img-fluid w-100" src="<?= base_url() ?>uploads/img/<?= $product->img?>" alt="">
                        </div>
                        <div class="text-center py-4">
                            <a class="h6 text-decoration-none text-truncate"
                                href="<?=base_url('game-detail/')?><?= $product->id ?? '' ?>"><?= $product->game_name??'' ?></a>
                            <div class="d-flex align-items-center justify-content-center mt-2">
                                <h5><?= $product->amount ?? '' ?><i class="fas fa-rupee-sign ml-1"></i></h5>
                                <h6 class="text-muted ml-2"><?= $product->points ?? '' ?><i
                                        class="fas fa-thin fa-star ml-1 text-primary"></i></h6>
                            </div>
                            <!-- <?php if (!empty($this->session->userdata('register_id'))) { ?>
                            <div class=" mb-4 pt-2">
                                <button class="btn btn-primary px-3" data-toggle="modal"
                                    data-target="#myModalPrice" onclick="pop_up_price('<?= $product->id??''?>')">Claim</button>
                            </div>
                            <?php } else { ?>
                            <div class=" mb-4 pt-2">
                                <button class="btn btn-primary px-3 disabled" data-toggle="modal"
                                    data-target="#myModal">Claim</button>
                            </div>
                            <?php } ?> -->
                        </div>
                    </div>
                </div>
                <!-- <div class="col-12">
                    <nav>
                        <ul class="pagination justify-content-center">
                            <li class="page-item disabled"><a class="page-link" href="#">Previous</span></a></li>
                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                        </ul>
                    </nav>
                </div> -->
                <?php } }else{ ?><div class="col-lg-12 col-md-6 col-sm-6 pb-1"><div class="text-center product-item bg-light p-4 mb-4" >Not Available</div></div><?php } ?>
            </div>
        </div>
        <!-- Shop Product End -->
    </div>
</div>
<!-- Shop End -->
<script>
function checkout_val() {
    var checkBox = $('input[type=radio]').is(':checked');

    if (checkBox == true) {
        $("#filters").removeClass('btn-disable');
    } else {
        $("#filters").addClass('btn-disable');
    }
};
</script>