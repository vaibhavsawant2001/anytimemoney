<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	
	public function __construct(){
		parent::__construct();
		$data = array();
		$this->load->model('Login_model');

	} 

	// Show login page
    public function index() {
		$this->load->view('login');
    }
    public function user_login()
    {
        $this->form_validation->set_rules('username','User Name','required');
        $this->form_validation->set_rules('password','User password','required');
    
        if($this->form_validation->run()){
            // grab user input
            $username = $this->security->xss_clean($this->input->post('username'));
            $password = $this->security->xss_clean($this->input->post('password'));
    
			 $data = array(
                'name' => $username,
                'password' => md5($password) 
			);
			
            $run = $this->Login_model->login_entry($data);
            if($run)
			{
				$this->session->set_flashdata('item','Insert data successfully'); 
				redirect(base_url('yaaaro_pms/Dashboard'));
			}
			else{
				$this->session->set_flashdata('incorrect','Incorrect username or password.'); 
				redirect(base_url('yaaaro_pms/login'));
			}
        }else{
            $this->session->set_flashdata('incorrect','Username and password required'); 
            redirect(base_url('yaaaro_pms/login'));
        }
    }
    
	
    public function logout(){
        $this->session->sess_destroy();
        redirect(base_url('yaaaro_pms'));
    }

}
