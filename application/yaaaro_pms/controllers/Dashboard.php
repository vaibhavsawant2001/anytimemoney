<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
	public $db;
	public function __construct()
	{
		parent::__construct();
		$data = array();
		$this->CI = &get_instance();
		$this->db_conn = $this->CI->db->conn_id;

	}

	public function index()
	{
		$data['alldata'] = $this->select_query('company_detail', [], 'asc', 1);

		$this->load->view('header');
		$this->load->view('dashboard', $data);
		$this->load->view('footer');
	}
	public function dashboard_edit($id)
	{
		$datas = $this->select_query('company_detail', ['id' => $id], '', '1', '');
		$data['row'] = $datas[0];

		$this->load->view('header');
		$this->load->view('dashboard_edit', $data);
		$this->load->view('footer');
	}
	public function dashboard_update()
	{

		$logo1 = $this->input->post('logo1');
		if (!empty($_FILES['logo']['name'])) {

			if (!empty($logo1)) {
				if (file_exists("../uploads/logo/" . $logo1)) {
					unlink("../uploads/logo/" . $logo1);
				}
			}

			$uploadPath = '../uploads/logo/';
			$file_allowed = 'jpg|jpeg|png|gif';
			$imgData = $this->files_upload($_FILES['logo'], $uploadPath, $file_allowed);
			if ($imgData == 'failed') {
				$this->session->set_flashdata('message', 'Something went wrong, try again later.');
				redirect(base_url('yaaaro_pms/dashboard/banner'));
				exit();
			}
		} else {
			$imgData = $logo1;
		}

		$this->form_validation->set_rules('name', 'Name Required', 'required');
		if ($this->form_validation->run()) {
			$editid = $this->security->xss_clean($this->input->post('id'));
			$name = $this->security->xss_clean($this->input->post('name'));
			$ceo = $this->security->xss_clean($this->input->post('ceo'));
			$address = $this->security->xss_clean($this->input->post('address'));
			$email = $this->security->xss_clean($this->input->post('email'));
			$phone = $this->security->xss_clean($this->input->post('phone'));
			$facebook = $this->security->xss_clean($this->input->post('facebook'));
			$twitter = $this->security->xss_clean($this->input->post('twitter'));
			$instagram = $this->security->xss_clean($this->input->post('instagram'));
			$google_plus = $this->security->xss_clean($this->input->post('google_plus'));

			$data = array(
				'name' => $name,
				'ceo' => $ceo,
				'address' => $address,
				'logo' => $imgData,
				'email' => $email,
				'phone' => $phone,
				'facebook' => $facebook,
				'twitter' => $twitter,
				'instagram' => $instagram,
				'google_plus' => $google_plus,
				'date' => date('Y-m-d'),
				'status' => '1'
			);

			$run = $this->update_query('company_detail', $editid, $data);

			if ($run) {
				$this->session->set_flashdata('messageadd', ' Update Successfully.');
				redirect(base_url('yaaaro_pms/dashboard'));
			} else {
				$this->session->set_flashdata('messageadd', 'Updated error.');
				redirect(base_url('yaaaro_pms/dashboard'));
			}

		}
	}

	//------------------------------------------banner controller-------------------------------
	public function banner()
	{
		$data['allbanner'] = $this->select_query('banner', [], 'asc');

		$this->load->view('header');
		$this->load->view('banner', $data);
		$this->load->view('footer');
	}

	public function banner_edit($id)
	{
		$datas = $this->select_query('banner', ['id' => $id], '', '1', '');
		$data['row'] = $datas[0];

		$this->load->view('header');
		$this->load->view('banner_add', $data);
		$this->load->view('footer');
	}

	public function banner_update()
	{
		$editid = $this->input->post('id');
		$this->form_validation->set_rules('title1', 'Title Required', 'required');

		$image1 = $this->input->post('image1');
		if (!empty($_FILES['image']['name'])) {

			if (!empty($image1)) {
				if (file_exists("../uploads/banner/" . $image1)) {
					unlink("../uploads/banner/" . $image1);
				}
			}

			$uploadPath = '../uploads/banner/';
			$file_allowed = 'jpg|jpeg|png|gif';
			$imgData = $this->files_upload($_FILES['image'], $uploadPath, $file_allowed);
			if ($imgData == 'failed') {
				$this->session->set_flashdata('message', 'Something went wrong, try again later.');
				redirect(base_url('yaaaro_pms/dashboard/banner'));
				exit();
			}
		} else {
			$imgData = $image1;
		}

		$data = array(
			'image' => $imgData,
			'date' => date('Y-m-d'),
			'status' => '1'
		);

		$run = $this->update_query('banner', $editid, $data);

		if ($run) {
			$this->session->set_flashdata('message', 'Banner Updated Successfully.');
			redirect(base_url('yaaaro_pms/dashboard/banner'));
		} else {
			$this->session->set_flashdata('message', 'Updated error.');
			redirect(base_url('yaaaro_pms/dashboard/banner'));
		}
	}

	public function banner_status()
	{
		$id = $this->input->get('id');
		$status = $this->input->get('status');

		$data = ['status' => $status];
		$this->update_query('banner', $id, $data);
	}

	// ==============================Category===================================
	public function category()
	{
		$data['alldata'] = $this->select_query('category', [], 'asc');
		$this->load->view('header');
		$this->load->view('category', $data);
		$this->load->view('footer');
	}

	public function category_update()
	{

		$editid = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('id')));
		$this->form_validation->set_rules('category', 'Category Required', 'required');
		$category = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('category')));

		if ($editid) {
			$data = array(
				'category' => $category,
			);

			$run = $this->update_query('category', $editid, $data);

			if ($run) {
				$this->session->set_flashdata('messageadd', 'Category Updated Successfully.');
				redirect(base_url('yaaaro_pms/dashboard/category'));
			} else {
				$this->session->set_flashdata('messageadd', 'Updated error.');
				redirect(base_url('yaaaro_pms/dashboard/category'));
			}

		}
	}

	public function category_edit($id)
	{
		$datas = $this->select_query('category', ['id' => $id], '', '1', '');
		$data['row'] = $datas[0];

		$this->load->view('header');
		$this->load->view('category_add', $data);
		$this->load->view('footer');
	}

	// ---------------------------------------Approval--------------------------------------
	public function approval()
	{
		$data['alldata'] = $this->select_query('my_product', [], 'desc');

		$this->load->view('header');
		$this->load->view('approval', $data);
		$this->load->view('footer');
	}

	public function Approval_status()
	{
		$id = $this->input->get('id');
		$status = $this->input->get('status');

		$data = ['status' => $status];
		$this->update_query('my_product', $id, $data);

		$my_product = $this->select_query('my_product', ['id' => $id], '', 1);
		$user_data = $this->select_query('register', ['register_id' => $my_product[0]['user_id']], '', 1);

		$new_jems = intval($user_data[0]['total_jems']) + intval($my_product[0]['jems']);
		$datas = ['total_jems' => $new_jems];

		$this->update_query('register', $my_product[0]['user_id'], $datas, 'register_id');

		$logs_data = array(
			'user_id' => $my_product[0]['user_id'] ?? '',
			'product_id' => $my_product[0]['product_id'] ?? '',
			'jems' => $my_product[0]['jems'] ?? '',
			'status' => (!empty($status)) ? ($status == 1) ? 'Approved By Admin' : 'Not Approved By Admin' : '',
		);

		$this->insert_query('logs', $logs_data);
	}

	public function Approval_comment()
	{
		$id = $this->input->get('id');
		$comment = $this->input->get('comment');

		$data = ['comment' => $comment];
		$this->update_query('my_product', $id, $data);
	}

	//------------------------------------------seo controller-------------------------------
	public function seo()
	{
		$data['allseo'] = $this->select_query('seo', [], 'asc');

		$this->load->view('header');
		$this->load->view('seo', $data);
		$this->load->view('footer');
	}

	public function seo_edit($id)
	{
		$datas = $this->select_query('seo', ['id' => $id], '', '1', '');
		$data['row'] = $datas[0];

		$this->load->view('header');
		$this->load->view('seo_edit', $data);
		$this->load->view('footer');
	}

	public function seo_update()
	{
		$editid = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('id')));
		$page_title = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('page_title')));
		$metatag = $this->input->post('metatag');

		$data = array(
			'page_title' => $page_title,
			'metatag' => $metatag,
			'date' => date('Y-m-d'),
			'status' => '1'
		);

		$run = $this->update_query('seo', $editid, $data);

		if ($run) {
			$this->session->set_flashdata('messageadd', 'seo Updated Successfully.');
			redirect(base_url('yaaaro_pms/dashboard/seo'));
		} else {
			$this->session->set_flashdata('messageadd', 'Updated error.');
			redirect(base_url('yaaaro_pms/dashboard/seo'));
		}
	}

	public function seo_status()
	{
		$id = $this->input->POST('id');
		$status = $this->input->POST('status');

		$data = [
			'status' => $status
		];

		$this->update_query('seo', $id, $data);
	}

	//------------------------------------------Contact controller-------------------------------
	public function contact()
	{
		$data['allcnts'] = $this->select_query('contact', ['type' => 'contact'], 'desc');

		$this->load->view('header');
		$this->load->view('contact', $data);
		$this->load->view('footer');
	}

	public function cnt_del($id)
	{
		$run = $this->delete_query('contact', $id);

		if ($run) {
			$this->session->set_flashdata('messageadd', 'Contact Delete Successfully.');
			redirect(base_url('yaaaro_pms/dashboard/contact'));
		} else {
			$this->session->set_flashdata('messageadd', 'Del error.');
			redirect(base_url('yaaaro_pms/dashboard/contact'));
		}
	}


	// ======================================Product=====================================
	public function product()
	{
		$data['alldata'] = $this->select_query('products', [], 'desc', '');
		$this->load->view('header');
		$this->load->view('product', $data);
		$this->load->view('footer');
	}

	public function product_add()
	{
		$data['categories'] = $this->select_query('category', [], 'asc');

		$this->load->view('header');
		$this->load->view('product_add', $data);
		$this->load->view('footer');
	}

	public function product_update()
	{
		$editid = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('id')));
		$product = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('product')));
		$category = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('category')));
		$amount = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('amount')));
		$jems = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('jems')));
		$short_desc = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('short_desc')));
		$long_desc = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('long_desc')));

		$image1 = $this->input->post('image1');
		if (!empty($_FILES['image']['name'])) {

			if (!empty($image1)) {
				if (file_exists("../uploads/img/" . $image1)) {
					unlink("../uploads/img/" . $image1);
				}
			}

			$uploadPath = '../uploads/img/';
			$file_allowed = 'jpg|jpeg|png';
			$imgData = $this->files_upload($_FILES['image'], $uploadPath, $file_allowed);
			if ($imgData == 'failed') {
				$this->session->set_flashdata('message', 'Something went wrong, try again later.');
				redirect(base_url('yaaaro_pms/dashboard/product'));
				exit();
			}
		} else {
			$imgData = $image1;
		}

		if ($editid) {
			$data = array(
				'product_name' => $product,
				'category_id' => $category,
				'amount' => $amount,
				'jems' => $jems,
				'short_desc' => $short_desc,
				'long_desc' => $long_desc,
				'img' => $imgData,
			);

			$run = $this->update_query('products', $editid, $data);

			if ($run) {
				$this->session->set_flashdata('message', 'Product Updated Successfully.');
				redirect(base_url('yaaaro_pms/dashboard/product'));
			} else {
				$this->session->set_flashdata('message', 'Updated error.');
				redirect(base_url('yaaaro_pms/dashboard/product'));
			}

		} else {

			$data = array(
				'product_name' => $product,
				'category_id' => $category,
				'amount' => $amount,
				'jems' => $jems,
				'short_desc' => $short_desc,
				'long_desc' => $long_desc,
				'img' => $imgData,
			);

			$run = $this->insert_query('products', $data);

			if ($run) {
				$this->session->set_flashdata('message', 'Product Add Successfully.');
				redirect(base_url('yaaaro_pms/dashboard/product'));
			} else {
				$this->session->set_flashdata('message', 'Add error.');
				redirect(base_url('yaaaro_pms/dashboard/product'));
			}
		}
	}

	public function product_edit($id)
	{
		$data['categories'] = $this->select_query('category', [], 'asc');
		$datas = $this->select_query('products', ['id' => $id], '', '1', '');
		$data['row'] = $datas[0];
		$this->load->view('header');
		$this->load->view('product_add', $data);
		$this->load->view('footer');
	}

	public function product_status()
	{
		$id = $this->input->get('id');
		$status = $this->input->get('status');
		$data = [
			'status' => $status
		];

		$this->update_query('products', $id, $data);
	}

	public function product_del($id)
	{
		$run = $this->delete_query('products', $id);

		if ($run) {
			$this->delete_query('fav_list', $id, 'car_id');

			$this->session->set_flashdata('messageadd', 'Product Deleted Successfully.');
			redirect(base_url('yaaaro_pms/dashboard/product'));
		} else {
			$this->session->set_flashdata('messageadd', 'Del error.');
			redirect(base_url('yaaaro_pms/dashboard/product'));
		}
	}

	// ======================================Prize=====================================
	public function prize()
	{
		$data['alldata'] = $this->select_query('games', [], 'desc', '');
		$this->load->view('header');
		$this->load->view('prize', $data);
		$this->load->view('footer');
	}

	public function prize_add()
	{
		$this->load->view('header');
		$this->load->view('prize_add');
		$this->load->view('footer');
	}

	public function prize_update()
	{
		$editid = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('id')));
		$prize = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('prize')));
		$amount = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('amount')));
		$points = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('points')));
		$short_desc = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('short_desc')));
		$long_desc = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('long_desc')));

		$image1 = $this->input->post('image1');
		if (!empty($_FILES['image']['name'])) {

			if (!empty($image1)) {
				if (file_exists("../uploads/img/" . $image1)) {
					unlink("../uploads/img/" . $image1);
				}
			}

			$uploadPath = '../uploads/img/';
			$file_allowed = 'jpg|jpeg|png';
			$imgData = $this->files_upload($_FILES['image'], $uploadPath, $file_allowed);
			if ($imgData == 'failed') {
				$this->session->set_flashdata('message', 'Something went wrong, try again later.');
				redirect(base_url('yaaaro_pms/dashboard/product'));
				exit();
			}
		} else {
			$imgData = $image1;
		}

		if ($editid) {
			$data = array(
				'game_name' => $prize,
				'amount' => $amount,
				'points' => $points,
				'short_desc' => $short_desc,
				'long_desc' => $long_desc,
				'img' => $imgData,
			);

			$run = $this->update_query('games', $editid, $data);

			if ($run) {
				$this->session->set_flashdata('message', 'Prize Updated Successfully.');
				redirect(base_url('yaaaro_pms/dashboard/prize'));
			} else {
				$this->session->set_flashdata('message', 'Updated error.');
				redirect(base_url('yaaaro_pms/dashboard/prize'));
			}

		} else {

			$data = array(
				'game_name' => $prize,
				'amount' => $amount,
				'points' => $points,
				'short_desc' => $short_desc,
				'long_desc' => $long_desc,
				'img' => $imgData,
			);

			$run = $this->insert_query('games', $data);

			if ($run) {
				$this->session->set_flashdata('message', 'Prize Add Successfully.');
				redirect(base_url('yaaaro_pms/dashboard/prize'));
			} else {
				$this->session->set_flashdata('message', 'Add error.');
				redirect(base_url('yaaaro_pms/dashboard/prize'));
			}
		}
	}

	public function prize_edit($id)
	{
		$datas = $this->select_query('games', ['id' => $id], '', '1', '');
		$data['row'] = $datas[0];
		$this->load->view('header');
		$this->load->view('prize_add', $data);
		$this->load->view('footer');
	}

	public function prize_status()
	{
		$id = $this->input->get('id');
		$status = $this->input->get('status');
		$data = [
			'status' => $status
		];

		$this->update_query('games', $id, $data);
	}

	public function prize_del($id)
	{
		$run = $this->delete_query('games', $id);

		if ($run) {
			$this->delete_query('fav_list', $id, 'car_id');

			$this->session->set_flashdata('messageadd', 'Prize Deleted Successfully.');
			redirect(base_url('yaaaro_pms/dashboard/prize'));
		} else {
			$this->session->set_flashdata('messageadd', 'Del error.');
			redirect(base_url('yaaaro_pms/dashboard/prize'));
		}
	}

	// =========================================Checkout_editor===========================
	public function checkout_desc($id)
	{
		$datas = $this->select_query('ck_editor_checkout', ['id' => $id], '', '1', '');
		$data['row'] = $datas[0];
		$this->load->view('header');
		$this->load->view('checkout_desc_add', $data);
		$this->load->view('footer');
	}
	public function checkout_editor()
	{
		$editid = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('id')));
		$message = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('message')));

		$data = array(
			'message' => $message,
		);

		$run = $this->update_query('ck_editor_checkout', $editid, $data);

		if ($run) {
			$this->session->set_flashdata('message', 'Checkout Description Updated Successfully.');
			redirect(base_url('yaaaro_pms/dashboard/checkout_desc/1'));
		} else {
			$this->session->set_flashdata('message', 'Updated error.');
			redirect(base_url('yaaaro_pms/dashboard/checkout_desc/1'));
		}
	}

	//=====================================user login=====================================
	public function user_login()
	{
		$data['alldata'] = $this->select_query('register', [], 'desc', '', 'register_id');
		$this->load->view('header');
		$this->load->view('user', $data);
		$this->load->view('footer');
	}

	public function user_logs($id)
	{
		$data['user_id'] = $id;
		$this->load->view('header');
		$this->load->view('user_logs', $data);
		$this->load->view('footer');
	}

	public function user_add()
	{
		$this->load->view('header');
		$this->load->view('user_add');
		$this->load->view('footer');
	}

	public function user_edit($id)
	{
		$datas = $this->select_query('register', ['register_id' => $id], '', '1', '');
		$data['row'] = $datas[0];
		$this->load->view('header');
		$this->load->view('user_add', $data);
		$this->load->view('footer');
	}

	public function user_status()
	{
		$id = $this->input->get('id');
		$status = $this->input->get('status');
		$data = [
			'status' => $status
		];

		$this->update_query('register', $id, $data, 'register_id');
	}

	public function user_del($id)
	{
		$run = $this->delete_query('register', $id, 'register_id');

		if ($run) {
			$this->delete_query('logs', $id, 'user_id');
			$this->delete_query('my_product', $id, 'user_id');
			$this->delete_query('my_price', $id, 'user_id');
			$this->delete_query('fav_list', $id, 'login_id');

			$this->session->set_flashdata('messageadd', 'User Deleted Successfully.');
			redirect(base_url('yaaaro_pms/dashboard/user_login'));
		} else {
			$this->session->set_flashdata('messageadd', 'Del error.');
			redirect(base_url('yaaaro_pms/dashboard/user_login'));
		}
	}

	public function user_update()
	{
		$editid = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('id')));
		$username = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('username')));
		$email = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('email')));
		$mobile = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('mobile')));
		$address = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('address')));
		$password = mysqli_real_escape_string($this->db_conn, $this->security->xss_clean($this->input->post('password')));

		if ($editid) {
			$data = array(
				'name' => $username,
				'email' => $email,
				'mobile' => $mobile,
				'city' => $address,
				'passwords' => $password,
				'password' => md5($password),
			);

			$run = $this->update_query('register', $editid, $data, 'register_id');

			if ($run) {
				$this->session->set_flashdata('message', 'User Updated Successfully.');
				redirect(base_url('yaaaro_pms/dashboard/user_login'));
			} else {
				$this->session->set_flashdata('message', 'Updated error.');
				redirect(base_url('yaaaro_pms/dashboard/user_login'));
			}

		} else {

			$data = array(
				'name' => $username,
				'email' => $email,
				'mobile' => $mobile,
				'city' => $address,
				'passwords' => $password,
				'password' => md5($password),
			);

			$run = $this->insert_query('register', $data);

			if ($run) {
				$this->session->set_flashdata('message', 'User Add Successfully.');
				redirect(base_url('yaaaro_pms/dashboard/user_login'));
			} else {
				$this->session->set_flashdata('message', 'Add error.');
				redirect(base_url('yaaaro_pms/dashboard/user_login'));
			}
		}
	}

	// ==================================== Common Functions ============================================
	public function files_upload($files, $file_Path, $file_allowed)
	{
		$_FILES['file']['name'] = $files['name'];
		$_FILES['file']['type'] = $files['type'];
		$_FILES['file']['tmp_name'] = $files['tmp_name'];
		$_FILES['file']['error'] = $files['error'];
		$_FILES['file']['size'] = $files['size'];

		// File upload configuration
		$new_name = time() . '_' . $files['name'];
		$config['file_name'] = $new_name;
		$uploadPath = $file_Path;
		$config['upload_path'] = $uploadPath;
		$config['allowed_types'] = $file_allowed;

		// Load and initialize upload library
		$this->load->library('upload', $config);
		$this->upload->initialize($config);

		// Upload file to server
		if ($this->upload->do_upload('file')) {
			$fileData = $this->upload->data();
			$uploadData['file_name'] = $fileData['file_name'];
			$uploadData['uploaded_on'] = date("Y-m-d H:i:s");
			$imgData = $fileData['file_name'];

			return $imgData;

		} else {
			return "failed";
		}
	}

	public function select_query($table, $where, $order_by = '', $limit = '', $order_by_id = '')
	{

		if (!empty($order_by)) {
			if (!empty($order_by_id)) {
				$this->db->order_by($order_by_id, $order_by);
			} else {
				$this->db->order_by('id', $order_by);
			}
		}

		$this->db->where($where);
		$this->db->from($table);
		if (!empty($limit)) {
			$this->db->limit($limit);
		}

		$query = $this->db->get()->result_array();

		return $query;
	}

	public function insert_query($table, $data)
	{
		return $this->db->insert($table, $data);
	}

	public function delete_query($table, $id, $other_parameter = '')
	{
		if (!empty($other_parameter)) {
			$this->db->where($other_parameter, $id);
		} else {
			$this->db->where('id', $id);
		}

		return $this->db->delete($table);
	}

	public function update_query($table, $id, $data, $other_parameter = '', $where = array())
	{
		if (!empty($where)) {
			$this->db->where($where);
		}

		if (!empty($other_parameter)) {
			$this->db->where($other_parameter, $id);
		} else {
			$this->db->where('id', $id);
		}

		return $this->db->update($table, $data);
	}



}