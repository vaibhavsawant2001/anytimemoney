<?php 
	class Crud extends CI_Controller 
{
	public function __construct()
	{
		parent::__construct();//call CodeIgniter's default Constructor
		$this->load->database();//load database libray manually
		$this->load->model('Crud_model');//load Model
	}

	public function csv_import(){
		$this->load->view('header');
		$this->load->view('import_data');
		$this->load->view('footer');
	}

	public function csv_importsuser(){
		$this->load->view('header');
		$this->load->view('import_datauser');
		$this->load->view('footer');
	}


	public function importdata()
	{ 
		if(isset($_POST["submit"]))
		{
			$file = $_FILES['file']['tmp_name'];
			$handle = fopen($file, "r");
			$c = 0;//
			while(($filesop = fgetcsv($handle, 1000, ",")) !== false)
			{
				$property_name = $filesop[0];
				$property_status = $filesop[1];
				$property_location = $filesop[2];
				$propert_sublocation = $filesop[3];
				$property_type = $filesop[4];
				$area = $filesop[5];
				$price = $filesop[6];
				$short_details = $filesop[7];
				$additional_details = $filesop[8];
				$youtube_video = $filesop[9];
				if($c<>0){					//SKIP THE FIRST ROW
					$this->Crud_model->saverecords($property_name,$property_status,$property_location,$propert_sublocation,$property_type,$area,$price,$short_details,$additional_details,$youtube_video);
				}
				$c = $c + 1;
			}
			$this->session->set_flashdata('message','Your File has been upload successfully'); 
				
				
		}
		redirect(base_url('yaaaro_pms/crud/csv_import'));
	}

	public function importdatausers()
	{ 
		if(isset($_POST["submit"]))
		{
			$file = $_FILES['file']['tmp_name'];
			$handle = fopen($file, "r");
			$c = 0;//
			while(($filesop = fgetcsv($handle, 1000, ",")) !== false)
			{
				$property_name = $filesop[0];
				$property_status = $filesop[1];
				$property_location = $filesop[2];
				$propert_sublocation = $filesop[3];
				$property_type = $filesop[4];
				$area = $filesop[5];
				$price = $filesop[6];
				$short_details = $filesop[7];
				$additional_details = $filesop[8];
				$youtube_video = $filesop[9];
				if($c<>0){					//SKIP THE FIRST ROW
					$this->Crud_model->saverecords2($property_name,$property_status,$property_location,$propert_sublocation,$property_type,$area,$price,$short_details,$additional_details,$youtube_video);
				}
				$c = $c + 1;
			}
			$this->session->set_flashdata('message','Your File has been upload successfully'); 
				
				
		}
		redirect(base_url('yaaaro_pms/crud/csv_importsuser'));
	}
    	
}
?>