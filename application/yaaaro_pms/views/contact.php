
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <h1>
     Contact
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url('yaaaro_pms/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Contact</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
        <div class="box-body table-responsive no-padding">
        <label class="label label-success text-center"><?= $this->session->flashdata('messageadd');?></label>
            <table class="table table-hover" id="datatable">
            <thead>
              <tr>
                <th hidden> ID </th>
                <th> Name </th> 
                <th> Email </th>              
                <th> Message </th> 
                <th> Subject </th> 
                <!-- <th> Ip Address </th> 
                <th> Page Url </th>      -->     
                <th> Delete </th>            
              </tr>
              </thead>
              <tbody>
              <?php 
              foreach($allcnts as $row)
              {
              ?>
              <tr>

                <td hidden> <?= $id; ?> </td>
                <td><?= $row['name']; ?></td>
                <td><?= $row['email']; ?></td>
                <td><?= $row['message']; ?></td>
                <td><?= $row['subject']; ?></td>
               <!--  <td><?= $row['ip_address']; ?></td>
                <td><?= $row['Page_url']; ?></td> -->
                  <td> 
                      <a href="<?= base_url('yaaaro_pms/dashboard/cnt_del/'.$row['id']);?>" onclick="return confirm('Do you want to delete?');" class='btn btn-danger'><i class='fa fa-trash-o'></i></button></td>
                  </td> 
              </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </div>      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 


