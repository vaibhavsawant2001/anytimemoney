<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Prize
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= base_url('yaaaro_pms/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Prize</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box">
      <a href="<?= base_url('yaaaro_pms/dashboard/prize_add'); ?>" class="btn btn-info"><i class='fa fa-plus'></i> Add
        Prize</a>
      <div class="box-body table-responsive no-padding">
        <label class="label label-success text-center">
          <?php
          echo $this->session->flashdata('messageadd');
          ?>
        </label>
        <table class="table table-hover" id="datatable">
          <thead>
            <tr>
              <th> Sr No. </th>
              <th>Prize Name</th>
              <th>Amount</th>
              <th>Points</th>
              <th>Image</th>
              <th> Status</th>
              <th> Edit </th>
              <th> Delete </th>
            </tr>
          </thead>
          <tbody>
            <?php
            $i = 1;
            foreach ($alldata as $row) {
              ?>
              <tr class="text-break">
                <td>
                  <?= $i++ ?>
                </td>
                <td class="text-capitalize text-break">
                  <?= $row['game_name'] ?? ''; ?>
                </td>
                <td>
                  <?= $row['amount'] ?? ''; ?>
                </td>
                <td>
                  <?= $row['points'] ?? ''; ?>
                </td>
                <td>
                  <img src="<?= base_url().'uploads/img/'.$row['img'] ?? ''; ?>" width="100px" height="100px">
                </td>
                <td>
                  <?php
                  if ($row['status'] == 1) {
                    echo '<label class="switch ">
                        <input type="checkbox" value=' . $row['id'] . ' name="status" class="primary status" checked>
                        <span class="slider"></span>
                      </label>';
                  } else {
                    echo '<label class="switch ">
                        <input type="checkbox" value=' . $row['id'] . ' name="status" class="primary status">
                        <span class="slider"></span>
                      </label>';
                  }
                ?>
                </td>
                <td>
                  <a href="<?= base_url('yaaaro_pms/dashboard/prize_edit/' . $row['id']); ?>"
                    class='btn btn-info'><i class='fa fa-pencil-square-o'></i></a>
                </td>
                <td>
                  <a href="<?= base_url('yaaaro_pms/dashboard/prize_del/' . $row['id']); ?>"
                    onclick="return confirm('Do you want to delete?');" class='btn btn-danger'><i
                      class='fa fa-trash-o'></i></button>
                </td>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
        <?php //echo $links; ?>

      </div>
    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  $(document).ready(function () {
    $(".status").change(function () {
      var a = $(this).is(':checked');
      var stid = $(this).val();
      console.log(stid);
      console.log(a);

      if (a == true)
        var status = 1;
      else
        var status = 0;

      $.ajax({
        url: "<?php echo base_url('yaaaro_pms/dashboard/prize_status'); ?>",
        type: "get",
        data: { id: stid, status: status },
        success: function (data) {

          console.log(data);
          alert("Status updated successfully");
        }
      });
    });
  });
</script>