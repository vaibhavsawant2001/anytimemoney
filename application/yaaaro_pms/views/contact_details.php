  <!-- ====================== Body  ========================= -->
 
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <h1>
        Contact Details
    </h1>
      <ol class="breadcrumb">
        <li><a href="admin.php"> <i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#"><i class="fa fa-dashboard"></i> Contact </a></li>
        <li class="active"> Edit Contact </li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
             <form enctype="multipart/form-data" action="<?= base_url();?>yaaaro_pms/dashboard/getin_update" method="POST">  
             <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">  
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1"> Title:- </label>
                    <input type="hidden" name="id" value="<?= @$contact['id']; ?>" >
                    <input type="text" name="title" class="form-control" value= "<?= @$contact['title'];; ?>" placeholder="Title" >
                  </div> 
                  <div class="form-group">
                    <label for="exampleInputEmail1"> Heading:- </label>
                    <input type="text" name="heading" class="form-control" value= "<?= @$contact['heading'];; ?>" placeholder="Heading" >
                  </div>
                   <div class="form-group">
                    <label for="exampleInputEmail1"> Content:- </label>
                    <textarea name="content" class="form-control" rows="4"> <?= @$contact['content']; ?></textarea>                  </div>  
                   <div class="form-group">
                    <label for="exampleInputFile"> Image :-  </label> <br />
                     <input type="hidden" name="image1" value="<?= @$contact['img']; ?>">
                     <img src="<?= base_url(); ?>uploads/contact/<?= @$contact['img']; ?>"  height="100" width="100px"/> <br /> <br />
                     <input type="file" name="image" size="12" id="image"  data-toggle="tooltip"  data-placement="top" title="For Better Result Use Width and Height as Mention Above"> 
                     <br><small>Only jpeg , jpg & png images allowed</small> 
                     <br><small><strong style="color:red">Size must be 302 × 167 px</strong></small> 
                  </div>   
                 
                </div>
                <!-- /.box-body -->
                <div class="box-footer" align="center">
                  <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
                </div>

              </form>
       </div>    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->