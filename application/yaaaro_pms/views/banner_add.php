<!-- ====================== Body  ========================= -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
  <section class="content-header">
        <h1>
        <?= (!empty($row['id'])) ? 'Banner Edit' : 'Banner Add' ?>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= base_url('yaaaro_pms/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="<?= base_url('yaaaro_pms/dashboard/banner'); ?>">Banner</a></li>
      <li class="active">
        <?= (!empty($row['id'])) ? 'Banner Edit' : 'Banner Add' ?>
      </li>
    </ol>
  </section>
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <form enctype="multipart/form-data" action="<?= base_url(); ?>yaaaro_pms/dashboard/banner_update"
                method="POST" id="enquiry_forms">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                    value="<?php echo $this->security->get_csrf_hash(); ?>">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputFile"> Image :- </label> <br />
                        <input type="hidden" name="id" value="<?= @$row['id']; ?>">
                        <input type="hidden" name="image1" value="<?= @$row['image']; ?>">
                        <?php if (!empty($row['id'])) { ?>
                        <img src="<?= base_url(); ?>uploads/banner/<?= @$row['image']; ?>" height="100" width="100px" />
                        <br />
                        <br />
                        <?php } ?>
                        <input type="file" name="image" size="12" id="image" data-toggle="tooltip" data-placement="top"
                            title="For Better Result Use Width and Height as Mention Above">
                        <br><small>Only jpeg , jpg & png images allowed</small>
                        <br><small><strong style="color:red">Size must be 1000 × 430 px</strong></small>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer" align="center">
                    <button type="submit" name="submit" value="submit" id="submit"
                        class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script type="text/javascript">
$("#submit").click(function(event) {
    if ($("#image").val() !== "") {
        var fileExtension = ['jpeg', 'jpg', 'png'];
        if ($.inArray($("#image").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
            alert("Only formats are allowed : " + fileExtension.join(', '));
            event.preventDefault();
        } else {
            $("#enquiry_forms").submit();
        }
    } else {
        <?php if (!empty($row['id'])) { ?>
        $("#enquiry_forms").submit();
        <?php } else { ?>
        alert("Image File is required");
        event.preventDefault();
        <?php } ?>
    }
});
</script>