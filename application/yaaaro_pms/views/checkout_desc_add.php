<?php
$urls = $this->uri->segment(3);

?>
<!-- ====================== Body  ========================= -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= (!empty($row['id'])) ? 'Prize Edit' : 'Prize Add' ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('yaaaro_pms/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?= base_url('yaaaro_pms/dashboard/prize'); ?>">Prize</a></li>
            <li class="active">
                <?= (!empty($row['id'])) ? 'Prize Edit' : 'Prize Add' ?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <form enctype="multipart/form-data" action="<?= base_url(); ?>yaaaro_pms/dashboard/checkout_editor"
                method="POST" id="enquiry_forms">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                    value="<?php echo $this->security->get_csrf_hash(); ?>">
                <div class="box-body">
                    <div class="form-group">
                        <input type="hidden" name="id" value="<?= @$row['id']; ?>">
                        <label for="exampleInputEmail1">Checkout Desc- </label>
                        <textarea name="message" id="summernote"
                            class="form-control"><?= @$row['message']; ?></textarea>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer" align="center">
                    <button type="submit" name="submit" id="submit" value="submit"
                        class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- summernote editor -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>