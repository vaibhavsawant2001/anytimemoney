<?php 
    $urls = $this->uri->segment(3);
    
?>
  <!-- ====================== Body  ========================= -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <h1>
         Category Edit
      </h1>
      <ol class="breadcrumb">
        <li><a href="admin.php"><i class="fa fa-dashboard" href="<?= base_url('yaaaro_pms/dashboard'); ?>"></i> Home</a></li>
        <li><a href="<?= base_url('yaaaro_pms/dashboard/category');?>">Category</a></li>
        <li class="active"> Category Edit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
             <form enctype="multipart/form-data" action="<?= base_url();?>yaaaro_pms/dashboard/category_update" method="POST" id="enquiry_forms">    
              <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
                <div class="box-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1"> Category- </label>
                    <input type="hidden" readonly name="id" value="<?= @$row['id']; ?>" >
                    <input type="text" name="category" class="form-control" value= "<?= @$row['category']; ?>" placeholder="Location" >
                  </div>    
                </div>
                <!-- /.box-body -->
                <div class="box-footer" align="center">
                  <button type="submit" name="submit" id="submit" value="submit" class="btn btn-primary">Submit</button>
                </div>

              </form>
       </div>    
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <script type="text/javascript">
    
    $("#submit").click(function (event) {
        if($("#image").val() !== "" ){
          var fileExtension = ['jpeg', 'jpg', 'png'];
          if ($.inArray($("#image").val().split('.').pop().toLowerCase(), fileExtension) == -1) {
              alert("Only formats are allowed : "+fileExtension.join(', '));
              event.preventDefault();
          }
          else{
                    $("#enquiry_forms").submit();
                }
          } 
          else{
                <?php if(!empty($row['id'])){ ?>
                $("#enquiry_forms").submit();
                <?php } else { ?> 
                   alert("Image File is required");
                    event.preventDefault();
                <?php } ?> 
          }
    });
</script>