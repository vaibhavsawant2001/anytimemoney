<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            User Logs
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('yaaaro_pms/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">User Logs</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <label class="label label-success text-center">
                    <?php
                    echo $this->session->flashdata('messageadd');
                    ?>
                </label>
                <table class="table table-hover" id="datatable">
                    <thead>
                        <tr>
                            <th scope="col">Date</th>
                            <th scope="col">Product</th>
                            <th scope="col">Amount</th>
                            <th scope="col">Jem</th>
                            <th scope="col">Point</th>
                            <th scope="col">Price</th>
                            <th scope="col">Status</th>
                            <th scope="col">Comment</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $users = $this->db->get_where('register', ['register_id' => $user_id])->result();
                        $transactions = $this->db->where_not_in('status', ('logout'))->get_where('logs', ['user_id' => $user_id])->result();
                        foreach ($transactions as $transaction) {
                            $price = $this->db->get_where('games', ['id' => $transaction->price_id])->result();
                            $product = $this->db->where_not_in('status', ('0'))->get_where('my_product', ['product_id' => $transaction->product_id])->result();
                            $products = $this->db->where_not_in('status', ('0'))->get_where('products', ['id' => $transaction->product_id])->result();
                            ?>
                            <tr>
                                <th scope="row">
                                    <?= date("d-m-Y", strtotime($transaction->created_at)) ?? '' ?>
                                </th>
                                <td class="text-capitalize text-break">
                                    <?= $products[0]->product_name ?? '' ?>
                                </td>
                                <td>
                                    <?= $transaction->amount ?? '' ?>
                                </td>
                                <td class="text-break">
                                    <?php if ($transaction->status == 'Approved By Admin') { ?>
                                        <?= $transaction->jems ?? '' ?>
                                    <?php } else { ?> 0
                                    <?php } ?>
                                </td>
                                <td>
                                    <?= $transaction->points ?? '' ?>
                                </td>
                                <td>
                                    <?= $price[0]->game_name ?? '' ?>
                                </td>
                                <td class="text-capitalize text-break">
                                    <?= $transaction->status ?? '' ?>
                                </td>
                                <td class="text-capitalize text-break">
                                    <?php if ($transaction->status !== 'Approval Request Sent') { ?>
                                        <?= $product[0]->comment ?? '' ?>
                                    <?php } ?>
                                </td>

                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
                <?php //echo $links; ?>

            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->