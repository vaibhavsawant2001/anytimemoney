<style type="text/css">
.company-detail {
    padding: 2% 5%;
    font-size: 16px;
}

.company-detail .image {
    margin-top: 10%;
}

.company-detail .image img {
    width: 50%;
}

.company-detail .time {
    margin-top: 5%;
}
</style>

<?php
foreach ($alldata as $data) {
    $id = $data['id'];
    $logo = $data['logo'];
    $name = $data['name'];
    $ceo = $data['ceo'];
    $address = $data['address'];
    $email = $data['email'];
    $phone = $data['phone'];
    $facebook = $data['facebook'];
    $twitter = $data['twitter'];
    $instagram = $data['instagram'];
    $google_plus = $data['google_plus'];
}
?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper" style="min-height=auto">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('yaaaro_pms/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <label class="label label-success text-center">
            <?= $this->session->flashdata('messageadd'); ?>
        </label>
        <!-- /.row -->
        <div class="box">
            <div class="col-md-12">
                <center>
                    <h2>Welcome to Yaaaro Management</h2>
                </center>
                <hr>
            </div>
            <div class="company-detail">
                <div class="row">
                    <div class="col-md-6">
                        <div class="image">
                            <img src="<?= base_url(); ?>uploads/logo/<?= $logo; ?>" alt="Company logo"
                                class="img-responsive">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <h3>Compnay Details</h3>
                        <p>
                            <strong>Company Name : </strong>
                            <?= $name; ?>
                        </p>
                        <p>
                            <strong>Company CEO : </strong>
                            <font color="blue">
                                <?= $ceo; ?>
                            </font>
                        </p>
                        <p>
                            <strong>Company Address : </strong>
                            <?= $address; ?>
                        </p>
                        <p>
                            <strong>Company Email Id : </strong>
                            <?= $email; ?>
                        </p>
                        <p>
                            <strong>Company Contact No. : </strong>
                            <?= $phone; ?>
                        </p>
                    </div>
                    <!-- <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <hr>
                                <h3>Social Media</h3>
                                <hr>
                            </div>
                            <div class="col-md-4">
                                <label>Facebook</label>
                                <p><?= $facebook; ?></p>
                            </div>
                            <div class="col-md-4">
                                <label>Instagram</label>
                                <p><?= $twitter; ?></p>
                            </div>
                            <div class="col-md-4">
                                <label>YouTube</label>
                                <p><?= $instagram; ?></p>
                            </div>
                             <div class="col-md-3">
                        <label>Google Plus</label>
                        <p><?= $google_plus; ?></p>
                      </div> 
                        </div>
                    </div> -->
                    <div class="col-md-12 " style="text-align: right;">
                        <a href="<?= base_url(); ?>yaaaro_pms/dashboard/dashboard_edit/<?= $id; ?>"
                            class="btn btn-success">Update Detail</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>