<style type="text/css">
.company-detail {
    padding: 2% 5%;
    font-size: 16px;
}

.company-detail .image {
    margin-top: 10%;
}

.company-detail .image img {
    width: 50%;
}

.company-detail .time {
    margin-top: 5%;
}
</style>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Dashboard
            <small>Control panel</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('yaaaro_pms/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <div class="box">
            <div class="col-md-12">
                <center>
                    <h2>Welcome to Yaaaro Management</h2>
                </center>
            </div>
            <form action="<?= base_url('yaaaro_pms/dashboard/dashboard_update'); ?>" method="post"
                enctype="multipart/form-data">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                    value="<?php echo $this->security->get_csrf_hash(); ?>">
                <div class="company-detail">
                    <div class="row">
                        <div class="col-md-6">
                            <input type="hidden" name="id" value=<?= $row['id']; ?>>
                            <div class="image">
                                <input type="hidden" name="logo1" value="<?= $row['logo']; ?>">
                                <img src="<?= base_url(); ?>uploads/logo/<?= $row['logo']; ?>" alt="Company logo"
                                    class="img-responsive"><br><br>
                                <input type="file" name="logo" style="margin-bottom: 4%">
                            </div>
                            <!-- <div class="row">
                                <h3>Social Media <font size="2">(insert like www.facebook.com?fhshdfk)</font>
                                </h3>
                                <div class="col-md-6">
                                    <label for="Facebook">Facebook</label>
                                    <input type="text" name="facebook" class="form-control"
                                        value="<?= $row['facebook']; ?>">
                                </div>
                                <div class="col-md-6">
                                    <label for="Twitter">Instagram</label>
                                    <input type="text" name="twitter" class="form-control"
                                        value="<?= $row['twitter']; ?>">
                                </div>
                                <div class="col-md-6">
                                    <label for="Instagram">YouTube</label>
                                    <input type="text" name="instagram" class="form-control"
                                        value="<?= $row['instagram']; ?>">
                                </div>
                                <div class="col-md-6" hidden>
                                    <label for="google_plus">Google Plus</label>
                                    <input type="text" name="google_plus" class="form-control"
                                        value="<?= $row['google_plus']; ?>">
                                </div>
                            </div> -->
                        </div>
                        <div class="col-md-6">
                            <h3>Compnay Details</h3>
                            <p>
                                <label for="Company Name">Company Name : </label>
                                <input type="text" name="name" class="form-control" value="<?= $row['name']; ?>">
                            </p>
                            <p>
                                <label for="Company CEO">Company CEO : </label>
                                <input type="text" name="ceo" class="form-control" value="<?= $row['ceo']; ?>">
                            </p>
                            <p>
                                <label for="Company Address">Company Address : </label>
                                <input type="text" name="address" class="form-control" value="<?= $row['address']; ?>">
                            </p>
                            <p>
                                <label for="Company Email Id">Company Email Id : </label>
                                <input type="text" name="email" class="form-control" value="<?= $row['email']; ?>">
                            </p>
                            <p>
                                <label for="Company Contact No.">Company Contact No. : </label>
                                <input type="text" name="phone" class="form-control" value="<?= $row['phone']; ?>">
                            </p>
                        </div>
                        <div class="col-md-12 " style="text-align: right;">
                            <button type="submit" name="submit" class="btn btn-success">Update Detail</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </section>
    <!-- /.content -->
</div>