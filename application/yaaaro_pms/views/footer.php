
<footer class="main-footer text-center">
   
    <p class="footerCopyright">
          Designed By  <a href="http://bwebdesignmumbai.in" target="_blank">Web Design Mumbai</a> 
          <a title="Privacy Policy" href="http://yaaaro.com/" target="_blank">yaaaro</a>
          Powered By <a href="http://bluesuninfo.com/" target="_blank">Blue sun info</a>
          <a href="http://placementmumbai.com/" target="_blank">PM </a>© 2019.
      </p>
  </footer>
</div>
  <!-- <div class="control-sidebar-bg"></div> -->
<!-- ./wrapper -->

<!-- jquery -->
<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script> -->

<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url(); ?>assets/admin/js/bootstrap.min.js"></script>

<!-- summernote editor -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>
<script>
  $('#summernote').summernote({
    placeholder: 'Enter Description',
    height: 200,
      followingToolbar: false
  });	
</script>

<!-- DataTables -->
<script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script>    
    $(document).ready( function () {
        $('#datatable').DataTable();
    } );
</script>


<!-- AdminLTE App -->
<script src="<?= base_url(); ?>assets/admin/js/adminlte.min.js"></script>

<script>  
  
    // sidebar menu 
      $(document).ready(function () {
          $('.sidebar-menu').tree()
      })

   // <!---outo hide alert box--->
   window.setTimeout(function() {
        $(".alert").fadeTo(500, 0).slideUp(500, function(){
            $(this).remove(); 
        });
      }, 4000);
  
</script>

</body>
</html>