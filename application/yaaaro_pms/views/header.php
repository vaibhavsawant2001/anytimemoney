<?php

if (!$this->session->userdata('username') && !$this->session->userdata('password'))
    redirect(base_url());

?>
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/admin/img/management.png">
    <title>Yaaaro PMS</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/admin/css/bootstrap.min.css" media="all">
    <style type="text/css">
        td {
            width: 500px;
            word-break: break-all;
        }

        .radios {
            margin-left: 3%;
        }
    </style>
    <!-- Theme style -->
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/admin/css/AdminLTE.min.css" media="all">
    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/admin/css/_all-skins.min.css" media="all">

    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"
        media="all">

    <!-- Google Font -->
    <link rel="stylesheet" type="text/css"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <link rel="stylesheet" type="text/css"
        href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


    <!-- summernote editor -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


</head>

<body class="hold-transition skin-blue sidebar-mini">
    <!-- Site wrapper -->
    <div class="wrapper">

        <header class="main-header">
            <!-- Logo -->
            <a href="<?= base_url(); ?>yaaaro_pms/dashboard" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini">
                    <img src="<?= base_url(); ?>assets/admin/img/management.png" class="logo-img" alt="User Image"
                        style="width: 210%; margin-left: -55%; margin-top: 20%;">
                    <!-- <b>Yaaaro</b> -->
                </span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">
                    <img src="<?= base_url(); ?>assets/admin/img/yaaaro.png" class="logo-img" alt="User Image"
                        style="height:35px">
                    <!-- <b>Yaaaro</b> -->
                </span>
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top">
                <!-- Sidebar toggle button-->
                <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>

                <div class="navbar-custom-menu">
                    <ul class="nav navbar-nav">
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <img src="<?= base_url(); ?>assets/admin/img/management.png" class="user-image"
                                    alt="User Image">
                                <span class="hidden-xs">Yaaaro PMS</span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header">
                                    <img src="<?= base_url(); ?>uploads/logo/logo.png" class="" alt="User Image">
                                    <p>
                                        Yaaaro Management System
                                    </p>
                                    <p>
                                        <a style="color: white;" href="https://bluesuninfo.com/"
                                            target="_blank">bluesuninfo.com</a>
                                    </p>
                                    <p>
                                        Contact No. : <a style="color: white;"
                                            href="tel:91-22-66154433">91-22-66154433</a>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <!-- <a href="change_password.php" class="btn btn-default btn-flat">Change Password</a> -->
                                    </div>
                                    <div class="pull-right">
                                        <a href="<?= base_url(); ?>yaaaro_pms/login/logout"
                                            class="btn btn-default btn-flat">Sign out</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>
        </header>

        <!-- =============================================== -->

        <!-- Left side column. contains the sidebar -->
        <aside class="main-sidebar">
            <!-- sidebar: style can be found in sidebar.less -->
            <section class="sidebar">

                <!-- sidebar menu: : style can be found in sidebar.less -->
                <ul class="sidebar-menu" data-widget="tree">
                    <!-- <li class="header">MAIN NAVIGATION</li> -->
                    <li class="#">
                        <a href="<?= base_url(); ?>yaaaro_pms/dashboard">
                            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="#">
                        <a href="<?= base_url(); ?>yaaaro_pms/dashboard/banner">
                            <i class="fa fa-image"></i> <span>Banner</span>
                        </a>
                    </li>
                    <li class="#">
                        <a href="<?= base_url(); ?>yaaaro_pms/dashboard/user_login">
                            <i class="fa fa-user"></i> <span>Users</span>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-folder"></i>
                            <span>Product</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li><a href="<?= base_url(); ?>yaaaro_pms/dashboard/product"><i
                                        class="fa fa-product-hunt"></i>
                                    Product</a></li>
                            <li><a href="<?= base_url(); ?>yaaaro_pms/dashboard/category"><i
                                        class="fa fa-list"></i>Category</a></li>
                            <li><a href="<?= base_url(); ?>yaaaro_pms/dashboard/checkout_desc/1"><i
                                        class="fa fa-info"></i>Checkout Description</a></li>
                            <li class="#">
                                <a href="<?= base_url(); ?>yaaaro_pms/dashboard/approval">
                                    <i class="fa fa-check"></i> <span>Approval</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                    <li class="#">
                        <a href="<?= base_url(); ?>yaaaro_pms/dashboard/prize">
                            <i class="fa fa-trophy"></i> <span>Prize</span>
                        </a>
                    </li>
                    <li><a href="<?= base_url(); ?>yaaaro_pms/dashboard/contact"><i class="fa fa-phone"></i> Contact</a>
                    </li>
                    <li class="#">
                        <a href="<?= base_url(); ?>yaaaro_pms/dashboard/seo">
                            <i class="fa fa-yoast"></i> <span>SEO</span>
                        </a>
                    </li>
            </section>
            <!-- /.sidebar -->
        </aside>
        <script type="text/javascript">
            function viewPassword() {
                var x = document.getElementById("txtPassword");
                if (x.type === "password") {
                    x.type = "text";
                } else {
                    x.type = "password";
                }
            }
        </script>}