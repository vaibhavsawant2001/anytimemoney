<?php
$urls = $this->uri->segment(3);

?>
<!-- ====================== Body  ========================= -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <?= (!empty($row['id'])) ? 'Prize Edit' : 'Prize Add' ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('yaaaro_pms/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?= base_url('yaaaro_pms/dashboard/prize'); ?>">Prize</a></li>
            <li class="active">
                <?= (!empty($row['id'])) ? 'Prize Edit' : 'Prize Add' ?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <form enctype="multipart/form-data" action="<?= base_url(); ?>yaaaro_pms/dashboard/prize_update"
                method="POST" id="enquiry_forms">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                    value="<?php echo $this->security->get_csrf_hash(); ?>">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Prize Name- </label>
                        <input type="hidden" name="id" value="<?= @$row['id']; ?>">
                        <input required type="text" name="prize" class="form-control" value="<?= @$row['game_name']; ?>"
                            placeholder="Prize Name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Amount- </label>
                        <input required type="number" name="amount" class="form-control" value="<?= @$row['amount']; ?>"
                            placeholder="Amount">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Points- </label>
                        <input required type="number" name="points" class="form-control" value="<?= @$row['points']; ?>"
                            placeholder="points">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"> short desc- </label>
                        <textarea name="short_desc" class="form-control"><?= @$row['short_desc']; ?></textarea>

                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Long desc- </label>
                        <textarea name="long_desc" id="summernote"
                            class="form-control"><?= @$row['long_desc']; ?></textarea>

                    </div>
                    <div class="form-group">
                        <label for="exampleInputFile"> Image :- </label> <br />
                        <input type="hidden" name="id" value="<?= @$row['id']; ?>">
                        <input type="hidden" name="image1" value="<?= @$row['img']; ?>">
                        <?php if (!empty($row['id'])) { ?>
                            <img src="<?= base_url(); ?>uploads/img/<?= @$row['img']; ?>" height="100" width="100px" />
                            <br />
                            <br />
                        <?php } ?>
                        <input type="file" name="image" size="12" id="image" data-toggle="tooltip" data-placement="top"
                            title="For Better Result Use Width and Height as Mention Above">
                        <br><small>Only jpeg , jpg & png images allowed</small>
                        <br><small><strong style="color:red">Size must be 500 × 500 px</strong></small>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer" align="center">
                    <button type="submit" name="submit" id="submit" value="submit"
                        class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<!-- summernote editor -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.12/summernote-lite.js"></script>