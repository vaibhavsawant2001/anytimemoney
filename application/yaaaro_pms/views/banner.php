
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
    <h1>
      Banner
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?= base_url('yaaaro_pms/dashboard');?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Banner</li>
      </ol>
    </section>
    <div class="container">
      <?php if(isset($_SESSION['message'])){ ?>
      <div class="alert alert-success alert-dismissible">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          <?= $this->session->flashdata('message');?>
      </div>
      <?php }?>
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="box">
          <div class="box-header text-center">              
          </div>
           <br>
        <div class="box-body table-responsive no-padding">
            <table class="table table-hover" id="datatable">
            <thead>
              <tr>
                <th hidden> ID </th>
                <th> Image </th> 
                <th> Status </th>            
                <th> Edit </th>            
              </tr>
              </thead>
              <tbody>
              <?php 
              foreach($allbanner as $row)
              {
              ?>
              <tr>
                <td hidden> <?= $id; ?> </td>
                <td>
                  <img src="<?= base_url(); ?>uploads/banner/<?= $row['image']; ?>"  height="100" width="100px"/>
                </td>
                <td>     
                  <?php 
                      if($row['status']==1){
                          echo '<label class="switch ">
                          <input type="checkbox" value='.$row['id'].' name="status" class="primary status" checked>
                          <span class="slider"></span>
                        </label>';
                      }else{
                          echo '<label class="switch ">
                          <input type="checkbox" value='.$row['id'].' name="status" class="primary status">
                          <span class="slider"></span>
                        </label>';
                      }
                        
                    ?>
                  </td>
                <td> 
                    <a href="<?= base_url('yaaaro_pms/dashboard/banner_edit/'.$row['id']);?>" class='btn btn-info'><i class='fa fa-pencil-square-o'></i></a>
                </td>  
              </tr>
              <?php } ?>
              </tbody>
            </table>
          </div>
        </div>      
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script>
    $(document).ready(function() {
    $(".status").change(function () {
		  var a = $(this).is(':checked');
			var stid = $(this).val(); 
			console.log(stid);
			console.log(a);
			
			  if(a==true)
	      var status = 1;
	      else
			  var status = 0;
		  
		  $.ajax({
          url: "<?php echo base_url('yaaaro_pms/dashboard/banner_status'); ?>",
          type: "get",
          data: {id: stid, status: status},
          success: function(data) {
					console.log(data);
					alert('Status updated successfully');
				}
				});
		    });
    });
</script>


