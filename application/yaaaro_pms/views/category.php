<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      Category
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= base_url('yaaaro_pms/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">Category</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box">
      <div class="box-body table-responsive no-padding">
        <label class="label label-success text-center">
          <?php
          echo $this->session->flashdata('messageadd');
          ?>
        </label>
        <table class="table table-hover" id="datatable">
          <thead>
            <tr>
              <th> Sr No. </th>
              <!-- <th> Location Id </th> -->
              <th> Category</th>
              <th> Edit </th>
            </tr>
          </thead>
          <tbody>
            <?php
            $i = 1;
            foreach ($alldata as $row) {
              ?>
              <tr>
                <td>
                  <?= $i++ ?>
                </td>
                <td>
                  <?= $row['category']; ?>
                </td>
                <td>
                  <a href="<?= base_url('yaaaro_pms/dashboard/category_edit/' . $row['id']); ?>" class='btn btn-info'><i
                      class='fa fa-pencil-square-o'></i></a>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
        <?php //echo $links; ?>

      </div>
    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->