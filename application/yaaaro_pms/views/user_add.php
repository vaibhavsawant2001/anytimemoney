<?php
$urls = $this->uri->segment(3);

?>
<!-- ====================== Body  ========================= -->

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        <?= (!empty($row['register_id'])) ? 'User Edit' : 'User Add' ?>
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('yaaaro_pms/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?= base_url('yaaaro_pms/dashboard/user_login'); ?>">User</a></li>
            <li class="active">
                <?= (!empty($row['register_id'])) ? 'User Edit' : 'User Add' ?>
            </li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <form enctype="multipart/form-data" action="<?= base_url(); ?>yaaaro_pms/dashboard/user_update"
                method="POST" id="enquiry_forms">
                <input type="hidden" name="<?php echo $this->security->get_csrf_token_name(); ?>"
                    value="<?php echo $this->security->get_csrf_hash(); ?>">
                <div class="box-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Name- </label>
                        <input type="hidden" name="id" value="<?= @$row['register_id']; ?>">
                        <input required type="username" name="username" class="form-control"
                            value="<?= @$row['name']; ?>" placeholder="Username">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Email- </label>
                        <input required type="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" name="email"
                            class="form-control" value="<?= @$row['email']; ?>" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Mobile- </label>
                        <input required type="tel" pattern="[1-9]{1}[0-9]{9}" name="mobile" class="form-control"
                            value="<?= @$row['mobile']; ?>" placeholder="Mobile">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Password- </label>
                        <input required type="text" name="password" class="form-control"
                            value="<?= @$row['passwords']; ?>" placeholder="Password">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1"> Address- </label>
                        <textarea required type="type" name="address"
                            class="form-control"><?= @$row['city']; ?></textarea>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer" align="center">
                    <button type="submit" name="submit" id="submit" value="submit"
                        class="btn btn-primary">Submit</button>
                </div>

            </form>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->