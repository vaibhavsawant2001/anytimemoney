

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
     Edit SEO
    </h1>
    <ol class="breadcrumb">
      <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="seo.php"> SEO</a></li>
      <li class="active">Edit SEO</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content"> 

    <div class="row">
      <!-- left column -->
      <div class="col-md-12">

        <div class="box box-primary">

          <div class="box-header with-border">

          </div>
          <!-- /.box-header -->
          <!-- form start -->
          <form action="<?= base_url();?>yaaaro_pms/dashboard/seo_update" method="POST">
            <input type="hidden" name="<?php echo $this->security->get_csrf_token_name();?>" value="<?php echo $this->security->get_csrf_hash();?>">
            <div class="box-body">
              <input type="hidden" class="form-control" name="id" value="<?= @$row['id']; ?>" >
              <div class="row">
                <div class="col-md-12 form-group">
                  <label for="Offer Type"> Page : <?= @$row['title']; ?></label>                  
                </div>
              </div>
              <div class="row">
                <div class="col-md-12 form-group">
                  <label for="Offer Type"> Page Title : </label>
                  <input type="text" class="form-control" name="page_title" value="<?= @$row['page_title']; ?>">
                </div>
              </div>
              
              <div class="col-md-12 form-group">
                <label for="Contant"> Metatag : </label>
                <textarea class="form-control" rows="4" name="metatag" placeholder='<meta name="description" content="Free Web utorials"><meta name="keywords" content="HTML,CSS"><meta name="author" content="John Doe">'><?= @$row['metatag']; ?></textarea>
              </div>
             <!-- /.box-body -->

             <div class="box-footer text-center">
               <button type="submit" name="submit" value="submit" class="btn btn-primary">Submit</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </section>
</div>

<?php include('footer.php'); ?>