<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Seo
        </h1>
        <ol class="breadcrumb">
            <li><a href="admin.php"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Seo</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-body table-responsive no-padding">
                <label class="label label-success text-center"><?= $this->session->flashdata('messageadd');?></label>
                <table class="table table-hover" id="datatable">
                    <thead>
                        <tr>
                            <th> Title </th>
                            <th>Page Title </th>
                            <th> Metatags </th>
                            <th> Status </th>
                            <th> </th>
                            <!-- <th> </th> -->
                        </tr>
                    </thead>
                    <tbody>
                        <?php  
                  
                            foreach($allseo as $data){ 
                                $id = $data['id'];
                                $title = $data['title'];
                                $page_title = $data['page_title'];
                                $metatag = $data['metatag'];
                                $status = $data['status'];                    
                        ?>
                        <tr>
                            <td> <?= $title; ?> </td>
                            <td> <?= $page_title; ?> </td>
                            <td> <textarea disabled rows="4" cols="100"><?= $metatag; ?></textarea> </td>


                            <td>     
                                <?php 
                                    if($status==1){
                                        echo '                 <label class="switch ">
                                        <input type="checkbox" value='.$id.' name="status" class="primary status" checked>
                                        <span class="slider"></span>
                                        </label>';
                                    }else{
                                        echo '                 <label class="switch ">
                                        <input type="checkbox" value='.$id.' name="status" class="primary status">
                                        <span class="slider"></span>
                                        </label>';
                                    }
                                        
                                    ?>
                            </td>

                            <td>
                                <a href="<?= base_url('yaaaro_pms/dashboard/seo_edit/'.$id);?>" class='btn btn-info'><i class='fa fa-pencil-square-o'></i></a>
                            </td>
                        </tr>
                        <?php }  ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<script>
    $(document).ready(function() {
      	$(".status").change(function () {
		    var a = $(this).is(':checked');
			var stid = $(this).val(); 
			console.log(stid);
			console.log(a);
			
			  if(a==true)
	      var status = 1;
	      else
			  var status = 0;
		  
		  $.ajax({
          url: "<?php echo base_url('yaaaro_pms/dashboard/seo_status'); ?>",
          type: "post",
          data: {id: stid, status: status},
          success: function(data) {
					console.log(data);
					if(data)
					// alert("Status updated successfully");
          console.log('Status updated successfully');
				    else
					// alert("server error");
          console.log('server error');
				}
				});
		    });
    });
</script>