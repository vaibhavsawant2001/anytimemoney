<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <section class="content-header">
    <h1>
      User
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?= base_url('yaaaro_pms/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
      <li class="active">User</li>
    </ol>
  </section>

  <!-- Main content -->
  <section class="content">
    <div class="box">
      <a href="<?= base_url('yaaaro_pms/dashboard/user_add'); ?>" class="btn btn-info"><i class='fa fa-plus'></i> Add
        User</a>
      <div class="box-body table-responsive no-padding">
        <label class="label label-success text-center">
          <?php
          echo $this->session->flashdata('messageadd');
          ?>
        </label>
        <table class="table table-hover" id="datatable">
          <thead>
            <tr>
              <th> Sr No. </th>
              <th>User Id</th>
              <th>User name</th>
              <th>User Email</th>
              <th> Password</th>
              <th> Mobile</th>
              <th> Total Gems</th>
              <th> Total Points</th>
              <!-- <th> Address</th> -->
              <th>User Logs</th>
              <th> Status</th>
              <th> Edit </th>
              <th> Delete </th>
            </tr>
          </thead>
          <tbody>
            <?php
            $i = 1;
            foreach ($alldata as $row) {
              ?>
              <tr class="text-break">
                <td>
                  <?= $i++ ?>
                </td>
                <td>
                  <?= $row['user_generated_id'] ?? ''; ?>
                </td>
                <td class="text-capitalize text-break">
                  <?= $row['name'] ?? ''; ?>
                </td>
                <td class="text-capitalize text-break">
                  <?= $row['email'] ?? ''; ?>
                </td>
                <td class="text-capitalize text-break">
                  <?= $row['passwords'] ?? ''; ?>
                </td>
                <td>
                  <?= $row['mobile'] ?? ''; ?>
                </td>
                <td class="text-capitalize text-break">
                  <?= $row['total_jems'] ?? ''; ?>
                </td>
                <td class="text-capitalize text-break">
                  <?= $row['total_points'] ?? ''; ?>
                </td>
                <!-- <td>
                  <?//= $row['city'] ?? ''; ?>
                </td> -->
                <td>
                  <a href="<?= base_url('yaaaro_pms/dashboard/user_logs/' . $row['register_id']); ?>" class='btn btn-warning'><i
                      class='fa fa-history'></i></a>
                </td>
                <td>
                  <?php
                  if ($row['status'] == 1) {
                    echo '<label class="switch ">
                        <input type="checkbox" value=' . $row['register_id'] . ' name="status" class="primary status" checked>
                        <span class="slider"></span>
                      </label>';
                  } else {
                    echo '<label class="switch ">
                        <input type="checkbox" value=' . $row['register_id'] . ' name="status" class="primary status">
                        <span class="slider"></span>
                      </label>';
                  }

                  ?>
                </td>
                <td>
                  <a href="<?= base_url('yaaaro_pms/dashboard/user_edit/' . $row['register_id']); ?>"
                    class='btn btn-info'><i class='fa fa-pencil-square-o'></i></a>
                </td>
                <td>
                  <a href="<?= base_url('yaaaro_pms/dashboard/user_del/' . $row['register_id']); ?>"
                    onclick="return confirm('Do you want to delete?');" class='btn btn-danger'><i
                      class='fa fa-trash-o'></i></button>
                </td>
                </td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
        <?php //echo $links; ?>

      </div>
    </div>
  </section>
  <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<script>
  $(document).ready(function () {
    $(".status").change(function () {
      var a = $(this).is(':checked');
      var stid = $(this).val();
      console.log(stid);
      console.log(a);

      if (a == true)
        var status = 1;
      else
        var status = 0;

      $.ajax({
        url: "<?php echo base_url('yaaaro_pms/dashboard/user_status'); ?>",
        type: "get",
        data: { id: stid, status: status },
        success: function (data) {

          console.log(data);
          alert("Status updated successfully");
        }
      });
    });
  });
</script>