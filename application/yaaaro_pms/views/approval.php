<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Approval
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?= base_url('yaaaro_pms/dashboard'); ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Approval</li>
        </ol>
    </section>
    <div class="container">
        <?php if (isset($_SESSION['message'])) { ?>
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <?= $this->session->flashdata('message'); ?>
            </div>
        <?php } ?>
    </div>
    <!-- Main content -->
    <section class="content">
        <div class="box">
            <div class="box-header text-center">
            </div>
            <br>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover" id="datatable">
                    <thead>
                        <tr>
                            <th hidden> ID </th>
                            <th> User Id </th>
                            <th> User Name </th>
                            <th> Product Name </th>
                            <th> Product Amount </th>
                            <th> Jems </th>
                            <th> File </th>
                            <th> Status </th>
                            <th> Comment </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $rand = rand(1, 10000);
                        
                        foreach ($alldata as $row) {
                            $user = $this->db->get_where('register',['register_id' => $row['user_id']])->result_array();
                            $product = $this->db->get_where('products', ['id' => $row['product_id']])->result_array();
                        ?>
                            <tr>
                                <td hidden>
                                    <?= $id; ?>
                                </td>
                                <td>
                                    <?= $user[0]['user_generated_id'] ?? '' ?>
                                </td>
                                <td class="text-break">
                                    <?= $user[0]['name']??''?>
                                </td>
                                <td class="text-break">
                                    <?= $product[0]['product_name'] ?? '' ?>
                                </td>
                                <td>
                                    <?= $product[0]['amount'] ?? '' ?>
                                </td>
                                <td>
                                    <?= $product[0]['jems'] ?? '' ?>
                                </td>
                                <td class="text-break">
                                    <a class="btn btn-primary"
                                        href="<?= base_url(); ?>uploads/file/<?= $row['file']; ?>" download>Download Recipt</a>
                                </td>
                                <td>
                                <?php
                                    if ($row['status'] == 1) {
                                        echo '<button type="button" value="0"  name="status" class="btn btn-success">Approved</button>';
                                    } else {
                                        echo '<button type="button" value="1" data-id="' . $row['id'] . '" name="status" class="status btn btn-warning">Approve</button>';
                                    }

                                    ?>
                                </td>
                                <td>
                                    <textarea class="comment_<?= $row['id'] ?>" onfocusOut="comment('<?= $row['id']?>')"><?= $row['comment'] ?? '' ?></textarea>
                                </td>
                            </tr>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->
<script>
    function comment(product_id) {
        var a = product_id;
        var stid = $(".comment_"+a).val();
        
        $.ajax({
            url: "<?php echo base_url('yaaaro_pms/dashboard/Approval_comment'); ?>",
            type: "get",
            data: {
                id: a,
                comment: stid
            },
            success: function (data) {
                alert('comment added successfully');
            }
        });
    }

    $(document).ready(function () {


        $(".status").click(function () {
            var prompt = confirm('are you sure, you want to approve??');
            if (prompt) {
                var stid = $(this).val();
                var a = $(this).attr('data-id');

                $.ajax({
                    url: "<?php echo base_url('yaaaro_pms/dashboard/Approval_status'); ?>",
                    type: "get",
                    data: {
                        id: a,
                        status: stid
                    },
                    success: function (data) {
                        alert('Status updated successfully');
                        location.reload();
                    }
                });
            }
        });
    });
</script>