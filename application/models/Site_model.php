<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Site_model extends CI_Model {

	public function index()
	{
		
	}

	//------------------------------Contact form Model --------------------------
	public function contactSubmit($data)
	{
		return $this->db->insert('contact', $data);
	}

	//------------------------------Category form Model --------------------------
	public function select_category()
	{
		$this->db->limit(3);
		$query = $this->db->get('category');
		return $query->result_array();
	}

	//------------------------------seo Model --------------------------
	public function select_seo($page)
	{
		$this->db->where('title', $page);
		$query = $this->db->get('seo');
		return $query->result_array();
	}

	// ------------------------------Banner--------------------------------

	public function select_banners()
	{
		$this->db->where('status', '1');
		$query = $this->db->get('banner');
		return $query->result_array();
	}

	
}
