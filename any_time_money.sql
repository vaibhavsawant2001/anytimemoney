-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 25, 2023 at 05:45 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `any_time_money`
--

-- --------------------------------------------------------

--
-- Table structure for table `api_games`
--

CREATE TABLE `api_games` (
  `id` int(11) NOT NULL,
  `game` varchar(250) DEFAULT NULL,
  `status` tinyint(4) NOT NULL DEFAULT 0,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `image` text DEFAULT NULL,
  `date` timestamp NULL DEFAULT current_timestamp(),
  `status` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `banner`
--

INSERT INTO `banner` (`id`, `image`, `date`, `status`) VALUES
(1, '1690084592_carousel-1.jpg', '2023-07-22 18:30:00', '1'),
(13, '1690075518_carousel-2.jpg', '2023-07-22 18:30:00', '1'),
(18, '1690075543_carousel-3.jpg', '2023-07-22 18:30:00', '1');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `category` varchar(100) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp(),
  `update_date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`id`, `category`, `created_date`, `update_date`) VALUES
(1, 'category11', '2023-07-16 06:29:09', '2023-07-16 06:29:09'),
(2, 'category2', '2023-07-16 06:29:17', '2023-07-16 06:29:17'),
(3, 'category3', '2023-07-16 06:29:24', '2023-07-16 06:29:24');

-- --------------------------------------------------------

--
-- Table structure for table `ck_editor_checkout`
--

CREATE TABLE `ck_editor_checkout` (
  `id` int(11) NOT NULL,
  `message` longtext DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `ck_editor_checkout`
--

INSERT INTO `ck_editor_checkout` (`id`, `message`, `created_at`) VALUES
(1, 'dsddsfvvvjkjjkjjj <b>Feroz jkkkkk</b>', '2023-07-23 15:10:53');

-- --------------------------------------------------------

--
-- Table structure for table `company_detail`
--

CREATE TABLE `company_detail` (
  `id` int(11) NOT NULL,
  `logo` text DEFAULT NULL,
  `name` text DEFAULT NULL,
  `ceo` text DEFAULT NULL,
  `address` text DEFAULT NULL,
  `address2` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `phone` text DEFAULT NULL,
  `office_time` text DEFAULT NULL,
  `facebook` text DEFAULT NULL,
  `twitter` text DEFAULT NULL,
  `instagram` text DEFAULT NULL,
  `google_plus` text DEFAULT NULL,
  `date` text DEFAULT NULL,
  `status` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `company_detail`
--

INSERT INTO `company_detail` (`id`, `logo`, `name`, `ceo`, `address`, `address2`, `email`, `phone`, `office_time`, `facebook`, `twitter`, `instagram`, `google_plus`, `date`, `status`) VALUES
(1, '1690074802_download.jpg', 'HIGHRISE ESTATESdd', 'Head Offices', ' 190, Guru Hargovindji Rd, Shaheed Bhagat Singh Society, S B Singh Colony, J B Nagar, Andheri East, Mumbai, Maharashtra 400059', '2-A, Ground Floor, Krishna Dyeing Compound,  46, Old Nagardas Cross Road, Jain Derasar Road,  Andheri (East), Mumbai &ndash; 400069, India  (with prior appointment only) ', 'info@rightchoicecars.in', ' +91 98200 58877', '', '', '', '', '', '2023-07-23', '1');

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `name` text DEFAULT NULL,
  `email` text DEFAULT NULL,
  `subject` text DEFAULT NULL,
  `message` text DEFAULT NULL,
  `content` text DEFAULT NULL,
  `ip_address` varchar(300) DEFAULT NULL,
  `Page_url` varchar(300) DEFAULT NULL,
  `type` text DEFAULT NULL,
  `date` text DEFAULT NULL,
  `status` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `email`, `subject`, `message`, `content`, `ip_address`, `Page_url`, `type`, `date`, `status`) VALUES
(4, 'sf', 'd@gmail.com', 'dfg', 'fg', '', '::1', 'http://localhost/highriseeStates/contact_us', 'contact', '2021-01-22', '1'),
(5, 'vf', 'fddfg@gmail.com', 'fdf', 'gdfg', '', '::1', 'http://localhost/highriseeStates/contact_us', 'contact', '2021-01-22', '1'),
(6, 'ds', 'sd@gmail.com', 'f', 'd', '', '::1', 'http://localhost/highriseeStates/contact_us', 'contact', '2021-01-22', '1'),
(7, 'ds', 'sd@gmail.com', 'f', 'd', '', '::1', 'http://localhost/highriseeStates/contact_us', 'contact', '2021-01-22', '1'),
(8, 'ds', 'ds@gmail.com', 'df', 'dfds', '', '::1', 'http://localhost/highriseeStates/contact_us', 'contact', '2021-01-30', '1'),
(9, 'ds', 'sd@gmail.com', NULL, 'dsf', '', '::1', 'https://localhost/RightChoice/Site/contact_us', 'contact', '2021-03-11', '1'),
(10, 'ds', 'sdd@gmail.com', NULL, 'dssdsd', '', '::1', 'https://localhost/RightChoice/Site/contact_us', 'contact', '2021-03-11', '1'),
(12, 'ds', 'sdds@gmail.com', NULL, 'sdfds', '', '::1', 'https://localhost/RightChoice/Site/contact_us', 'contact', '2021-03-11', '1'),
(13, 'ds', 'dffd@gmail.com', NULL, 'dssd', '', '::1', 'https://localhost/RightChoice/Site/contact_us', 'contact', '2021-03-11', '1'),
(14, 'ds', 'sds@gmail.com', NULL, 'dss', '', '::1', 'https://localhost/RightChoice/Site/contact_us', 'contact', '2021-03-11', '1'),
(15, 'd', 'fdfd@gmail.com', NULL, 'dsf', '', '::1', 'https://localhost/RightChoice/Site/contact_us', 'contact', '2021-03-11', '1'),
(16, 'kibzBIMyONHDF', 'examaboueo@gmail.com', NULL, '5135695749', '', '194.53.178.135', 'https://www.rightchoicecars.com/Site/contact_us', 'contact', '2021-04-01', '1'),
(17, 'fZChmATaOGPrEIx', 'theivileszt@gmail.com', NULL, '6723886717', '', '113.172.210.41', 'https://www.rightchoicecars.com/Site/contact_us', 'contact', '2021-04-06', '1'),
(21, 'feroz', 'feroz@gmail.com', NULL, 'sdfdf', '', '::1', 'http://localhost/any-time-money/contact-us', 'contact', '2023-07-15', '1'),
(22, 'ds', 'df@gmail.com', NULL, 'fgf', '', '::1', 'http://localhost/any-time-money/contact-us', 'contact', '2023-07-15', '1');

-- --------------------------------------------------------

--
-- Table structure for table `fav_list`
--

CREATE TABLE `fav_list` (
  `id` int(11) NOT NULL,
  `login_id` int(11) DEFAULT NULL,
  `car_id` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT 1,
  `date` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- --------------------------------------------------------

--
-- Table structure for table `games`
--

CREATE TABLE `games` (
  `id` int(11) NOT NULL,
  `game_name` varchar(125) NOT NULL,
  `amount` bigint(20) NOT NULL,
  `points` bigint(11) DEFAULT NULL,
  `img` varchar(100) NOT NULL,
  `short_desc` mediumtext DEFAULT NULL,
  `long_desc` longtext DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0=inactive,1=active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `games`
--

INSERT INTO `games` (`id`, `game_name`, `amount`, `points`, `img`, `short_desc`, `long_desc`, `status`, `created_at`, `updated_at`) VALUES
(1, 'game-1', 15, 10, 'product-1.jpg', 'short desc', 'long desc', 1, '2023-07-17 02:47:04', '2023-07-17 02:47:04'),
(2, 'game-2', 30002, 20, 'product-2.jpg', 'short desc', 'long desc', 1, '2023-07-17 02:47:15', '2023-07-17 02:47:15'),
(3, 'game-3', 30000, 200, 'product-3.jpg', 'short desc', NULL, 1, '2023-07-17 02:47:30', '2023-07-17 02:47:30'),
(4, 'game-4', 24000, 200, 'product-4.jpg', NULL, 'long desc', 1, '2023-07-17 02:47:30', '2023-07-17 02:47:30'),
(5, 'game-5', 4000, 3424, 'product-5.jpg', 'short desc', 'long desc', 1, '2023-07-17 02:47:15', '2023-07-17 02:47:15');

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE `login` (
  `id` int(11) NOT NULL,
  `name` varchar(300) DEFAULT NULL,
  `password` varchar(300) DEFAULT NULL,
  `pass` varchar(300) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`id`, `name`, `password`, `pass`, `created_date`) VALUES
(1, 'bluesun', 'a60b0e99251c0729ede89b864bf0f33b', 'yaaaro', '2020-10-13 05:56:04'),
(2, 'bluesun1', '19ddcb797fc31baec5eb9b3ffcc75b87', 'yaaaro1', '2020-10-13 05:56:04');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

CREATE TABLE `logs` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `game_id` int(11) DEFAULT NULL,
  `price_id` int(11) DEFAULT NULL,
  `jems` bigint(20) DEFAULT 0,
  `points` bigint(20) DEFAULT 0,
  `new_points` bigint(20) DEFAULT 0,
  `amount` bigint(20) DEFAULT 0,
  `status` varchar(30) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `user_id`, `product_id`, `game_id`, `price_id`, `jems`, `points`, `new_points`, `amount`, `status`, `created_at`, `updated_at`) VALUES
(1, 0, 0, 0, 0, 0, 0, 0, 0, 'registration', '2023-07-23 15:07:54', '2023-07-23 15:07:54'),
(2, 0, 0, 0, 0, 0, 0, 0, 0, 'login', '2023-07-23 15:08:03', '2023-07-23 15:08:03'),
(3, 9, 7, 0, 0, 10, 0, 0, 100000, 'Approval Request Sent', '2023-07-23 15:11:04', '2023-07-23 15:11:04'),
(5, 9, 0, 0, 0, 0, 0, 0, 0, 'logout', '2023-07-23 15:27:27', '2023-07-23 15:27:27'),
(6, 9, 7, NULL, NULL, 10, 0, 0, 0, 'Approved By Admin', '2023-07-23 15:29:52', '2023-07-23 15:29:52'),
(7, 0, 0, 0, 0, 0, 0, 0, 0, 'login', '2023-07-23 15:31:05', '2023-07-23 15:31:05'),
(8, 9, 6, 0, 0, 30, 0, 0, 100000, 'Approval Request Sent', '2023-07-23 15:31:19', '2023-07-23 15:31:19'),
(9, 9, 6, NULL, NULL, 30, 0, 0, 0, 'Approved By Admin', '2023-07-23 15:32:09', '2023-07-23 15:32:09'),
(10, 9, 0, 0, 1, 0, 10, 20, 0, 'Claimed the price', '2023-07-23 16:17:41', '2023-07-23 16:17:41'),
(11, 0, 0, 0, 0, 0, 0, 0, 0, 'login', '2023-07-25 03:31:08', '2023-07-25 03:31:08'),
(12, 9, 0, 0, 0, 0, 0, 0, 0, 'logout', '2023-07-25 03:43:13', '2023-07-25 03:43:13'),
(13, 0, 0, 0, 0, 0, 0, 0, 0, 'login', '2023-07-25 03:43:35', '2023-07-25 03:43:35'),
(14, 9, 0, 0, 0, 0, 0, 0, 0, 'logout', '2023-07-25 03:43:40', '2023-07-25 03:43:40');

-- --------------------------------------------------------

--
-- Table structure for table `my_price`
--

CREATE TABLE `my_price` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `price_id` int(11) NOT NULL,
  `points` int(11) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0=inactive,1=active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `my_price`
--

INSERT INTO `my_price` (`id`, `user_id`, `price_id`, `points`, `status`, `created_at`, `updated_at`) VALUES
(10, 9, 1, 10, 1, '2023-07-23 16:17:41', '2023-07-23 16:17:41');

-- --------------------------------------------------------

--
-- Table structure for table `my_product`
--

CREATE TABLE `my_product` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `file` varchar(100) DEFAULT NULL,
  `jems` bigint(20) DEFAULT NULL,
  `comment` varchar(200) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 0 COMMENT '0=not approved,1=approved,2=rejected',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `my_product`
--

INSERT INTO `my_product` (`id`, `user_id`, `product_id`, `file`, `jems`, `comment`, `status`, `created_at`, `updated_at`) VALUES
(10, 9, 7, '1690125064_vendor-4.jpg', 10, 'request done', 1, '2023-07-23 15:11:04', '2023-07-23 15:11:04'),
(11, 9, 6, '1690126279_game-5.jpg', 30, 'correct', 1, '2023-07-23 15:31:19', '2023-07-23 15:31:19');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `product_name` varchar(100) NOT NULL,
  `amount` bigint(20) DEFAULT NULL,
  `jems` int(11) DEFAULT NULL,
  `img` varchar(100) DEFAULT NULL,
  `short_desc` mediumtext DEFAULT NULL,
  `long_desc` longtext DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '0=inactive,1=active',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `product_name`, `amount`, `jems`, `img`, `short_desc`, `long_desc`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Product 1', 500, 10, 'game-1.jpg', 'short desc', NULL, 1, '2023-07-16 07:37:13', '2023-07-16 07:37:13'),
(2, 1, 'Product 2', 10000, 100, 'game-2.jpg', NULL, 'long desc', 1, '2023-07-16 07:37:13', '2023-07-16 07:37:13'),
(3, 1, 'Product 3', 10000, 40, 'game-3.jpg', 'short desc', 'long desc', 1, '2023-07-16 07:37:13', '2023-07-16 07:37:13'),
(5, 1, 'Product 4', 100000, 60, 'game-5.jpg', NULL, 'long desc', 1, '2023-07-16 07:37:13', '2023-07-16 07:37:13'),
(6, 1, 'Product 5', 100000, 30, 'product-1.jpg', 'short desc', NULL, 1, '2023-07-16 07:37:13', '2023-07-16 07:37:13'),
(7, 1, 'Product 6', 100000, 10, 'game-6.jpg', NULL, 'long desc', 1, '2023-07-16 07:37:13', '2023-07-16 07:37:13'),
(8, 2, 'Product 7', 100000, 20, 'game-6.jpg', 'short desc', 'long desc', 1, '2023-07-16 07:37:13', '2023-07-16 07:37:13'),
(9, 2, 'Product 8', 100000, 2, 'game-5.jpg', 'short desc', 'long desc', 1, '2023-07-16 07:37:13', '2023-07-16 07:37:13');

-- --------------------------------------------------------

--
-- Table structure for table `register`
--

CREATE TABLE `register` (
  `register_id` int(11) NOT NULL,
  `user_generated_id` varchar(100) NOT NULL,
  `name` varchar(300) DEFAULT NULL,
  `email` varchar(300) DEFAULT NULL,
  `mobile` varchar(300) DEFAULT NULL,
  `city` varchar(300) DEFAULT NULL,
  `password` varchar(300) DEFAULT NULL,
  `passwords` varchar(300) DEFAULT NULL,
  `total_points` bigint(20) DEFAULT 0,
  `total_jems` bigint(20) DEFAULT 0,
  `status` int(11) DEFAULT 1,
  `date` timestamp NULL DEFAULT current_timestamp(),
  `ip_address` varchar(300) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `register`
--

INSERT INTO `register` (`register_id`, `user_generated_id`, `name`, `email`, `mobile`, `city`, `password`, `passwords`, `total_points`, `total_jems`, `status`, `date`, `ip_address`) VALUES
(9, '1690124874', 'rajmm', 'rajsharmmm@gmail.com', '8756452355', 'jkkj', '202cb962ac59075b964b07152d234b70', '123', 20, 40, 1, '2023-07-23 15:07:54', '::1');

-- --------------------------------------------------------

--
-- Table structure for table `seo`
--

CREATE TABLE `seo` (
  `id` int(11) NOT NULL,
  `title` text DEFAULT NULL,
  `page_title` text DEFAULT NULL,
  `metatag` text DEFAULT NULL,
  `date` text DEFAULT NULL,
  `status` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

--
-- Dumping data for table `seo`
--

INSERT INTO `seo` (`id`, `title`, `page_title`, `metatag`, `date`, `status`) VALUES
(1, 'Home', 'Home', '  <meta name=\"description\" content=\"yaaaro\" /> \r\n  <meta name=\"keywords\" content=\"website desin and development company in mumbai, web development, web design, seo\" /> ', '2020-01-04', '1'),
(6, 'Contact Us', 'Contact Us', '  <meta name=\"description\" content=\"contact us\" />   <meta name=\"keywords\" content=\"contact us\" /> ', '2021-01-22', '1'),
(8, 'Checkout', 'Checkout', '  <meta name=\"description\" content=\"contact us\" />   <meta name=\"keywords\" content=\"contact us\" /> ', '2021-01-22', '1'),
(10, 'my_shopping', 'My Shopping', '  <meta name=\"description\" content=\"contact us\" />   <meta name=\"keywords\" content=\"contact us\" /> ', '2021-01-22', '1'),
(11, 'Game', 'Games', '  <meta name=\"description\" content=\"contact us\" />   <meta name=\"keywords\" content=\"contact us\" /> ', '2021-01-22', '1'),
(12, 'Games_details', 'Games Details', '  <meta name=\"description\" content=\"contact us\" />   <meta name=\"keywords\" content=\"contact us\" /> ', '2021-01-22', '1'),
(13, 'Product', 'Shop', '  <meta name=\"description\" content=\"contact us\" />   <meta name=\"keywords\" content=\"contact us\" /> ', '2021-01-22', '1'),
(14, 'Products_details', 'Product Details', '  <meta name=\"description\" content=\"contact us\" />   <meta name=\"keywords\" content=\"contact us\" /> ', '2021-01-22', '1'),
(15, 'login', 'Login', '  <meta name=\"description\" content=\"contact us\" />   <meta name=\"keywords\" content=\"contact us\" /> ', '2021-01-22', '1'),
(16, 'fav', 'Your Favorite', '  <meta name=\"description\" content=\"contact us\" />   <meta name=\"keywords\" content=\"contact us\" /> ', '2021-01-22', '1'),
(17, 'forgot', 'Forget Password', '  <meta name=\"description\" content=\"contact us\" />   <meta name=\"keywords\" content=\"contact us\" /> ', '2021-01-22', '1'),
(18, 'register', 'Register', '  <meta name=\"description\" content=\"contact us\" />   <meta name=\"keywords\" content=\"contact us\" /> ', '2021-01-22', '1'),
(19, 'transaction', 'Transaction', '  <meta name=\"description\" content=\"contact us\" />   <meta name=\"keywords\" content=\"contact us\" /> ', '2021-01-22', '1');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `api_games`
--
ALTER TABLE `api_games`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ck_editor_checkout`
--
ALTER TABLE `ck_editor_checkout`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_detail`
--
ALTER TABLE `company_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `fav_list`
--
ALTER TABLE `fav_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `games`
--
ALTER TABLE `games`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login`
--
ALTER TABLE `login`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `logs`
--
ALTER TABLE `logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `my_price`
--
ALTER TABLE `my_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `my_product`
--
ALTER TABLE `my_product`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `register`
--
ALTER TABLE `register`
  ADD PRIMARY KEY (`register_id`);

--
-- Indexes for table `seo`
--
ALTER TABLE `seo`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `api_games`
--
ALTER TABLE `api_games`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ck_editor_checkout`
--
ALTER TABLE `ck_editor_checkout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `company_detail`
--
ALTER TABLE `company_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `fav_list`
--
ALTER TABLE `fav_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=71;

--
-- AUTO_INCREMENT for table `games`
--
ALTER TABLE `games`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `login`
--
ALTER TABLE `login`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `logs`
--
ALTER TABLE `logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `my_price`
--
ALTER TABLE `my_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `my_product`
--
ALTER TABLE `my_product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `register`
--
ALTER TABLE `register`
  MODIFY `register_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `seo`
--
ALTER TABLE `seo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
